package smu.ws.lottery.domain.strategy.model.res;

import smu.ws.lottery.common.Constants;
import smu.ws.lottery.domain.strategy.model.vo.DrawAwardVo;


public class DrawResult {

    // 用户ID
    private String uId;

    // 策略ID
    private Long strategyId;

    /**
     * 中奖状态 0未中奖，1已中奖，2兜底奖
     */

    private Integer drawState = Constants.DrawState.FAIL.getCode();

    /**
     * 中奖奖品信息
     */

    private DrawAwardVo drawAwardInfo;

    public DrawResult(String uId, Long strategyId, Integer drawState) {
        this.uId = uId;
        this.strategyId = strategyId;
        this.drawState = drawState;
    }

    public DrawResult(String uId, Long strategyId, Integer drawState, DrawAwardVo drawAwardInfo) {
        this.uId = uId;
        this.strategyId = strategyId;
        this.drawState = drawState;
        this.drawAwardInfo = drawAwardInfo;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Integer getDrawState() {
        return drawState;
    }

    public void setDrwaState(Integer drwaState) {
        this.drawState = drwaState;
    }

    public DrawAwardVo getDrawAwardInfo() {
        return drawAwardInfo;
    }

    public void setDrawAwardInfo(DrawAwardVo drawAwardInfo) {
        this.drawAwardInfo = drawAwardInfo;
    }
}
