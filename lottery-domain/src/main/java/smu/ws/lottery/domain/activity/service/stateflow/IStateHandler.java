package smu.ws.lottery.domain.activity.service.stateflow;

import smu.ws.lottery.common.Constants;
import smu.ws.lottery.common.Result;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 20:08
 * @description 状态处理接口
 */
public interface IStateHandler {

    /**
     * 活动提审
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result arraignment(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 审核通过
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result cheeckPass(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 审核拒绝
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result checkRefuse(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 撤审撤销
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result checkRevoke(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 活动关闭
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result close(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 活动开启
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result open(Long activityId, Enum<Constants.ActivityState> currentState);

    /**
     * 活动执行
     *
     * @param activityId   活动Id
     * @param currentState 当前状态
     * @return             审核结果
     */
    Result doing(Long activityId, Enum<Constants.ActivityState> currentState);

}
