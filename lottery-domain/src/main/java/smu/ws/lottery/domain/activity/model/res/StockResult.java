package smu.ws.lottery.domain.activity.model.res;

import smu.ws.lottery.common.Result;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/5/6 15:48
 * @description 库存处理结果
 */
public class StockResult extends Result {

    /**
     * 库存key
     */
   private String stockKey;
    /**
     * 库存剩余
     */
   private Integer stockSurplusCount;

    public StockResult(String code, String info) {
        super(code, info);
    }

    public StockResult(String code, String info, String stockKey, Integer stockSurplusCount) {
        super(code, info);
        this.stockKey = stockKey;
        this.stockSurplusCount = stockSurplusCount;
    }

    public String getStockKey() {
        return stockKey;
    }

    public void setStockKey(String stockKey) {
        this.stockKey = stockKey;
    }

    public Integer getStockSurplusCount() {
        return stockSurplusCount;
    }

    public void setStockSurplusCount(Integer stockSurplusCount) {
        this.stockSurplusCount = stockSurplusCount;
    }
}
