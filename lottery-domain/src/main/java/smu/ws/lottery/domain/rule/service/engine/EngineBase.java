package smu.ws.lottery.domain.rule.service.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smu.ws.lottery.common.Constants;
import smu.ws.lottery.domain.rule.model.aggregates.TreeRuleRich;
import smu.ws.lottery.domain.rule.model.req.DecisionMatterReq;
import smu.ws.lottery.domain.rule.model.res.EngineResult;
import smu.ws.lottery.domain.rule.model.vo.TreeNodeVo;
import smu.ws.lottery.domain.rule.model.vo.TreeRootVo;
import smu.ws.lottery.domain.rule.service.logic.LogicFilter;

import java.util.Map;


public abstract class EngineBase extends EngineConfig implements EngineFilter {

    private Logger logger = LoggerFactory.getLogger(EngineBase.class);

    @Override
    public EngineResult process(DecisionMatterReq matter) {
        throw new RuntimeException("未实现规则引擎服务");
    }

    protected TreeNodeVo engineDecisionMaker(TreeRuleRich treeRuleRich, DecisionMatterReq matter) {
        TreeRootVo treeRoot = treeRuleRich.getTreeRoot();
        Map<Long, TreeNodeVo> treeNodeMap = treeRuleRich.getTreeNodeMap();

        // 规则树根ID
        Long rootNodeId = treeRoot.getTreeRootNodeId();
        //根据树根ID查询树根的详细信息
        TreeNodeVo treeNodeInfo = treeNodeMap.get(rootNodeId);

        // 节点类型[NodeType]；1子叶、2果实
        while (Constants.NodeType.STEM.equals(treeNodeInfo.getNodeType())) {
            String ruleKey = treeNodeInfo.getRuleKey();
            LogicFilter logicFilter = logicFilterMap.get(ruleKey);
            String matterValue = logicFilter.matterValue(matter);
            Long nextNode = logicFilter.filter(matterValue, treeNodeInfo.getTreeNodeLineInfoList());
            treeNodeInfo = treeNodeMap.get(nextNode);
            logger.info("决策树引擎=>{} userId：{} treeId：{} treeNode：{} ruleKey：{} matterValue：{}", treeRoot.getTreeName(), matter.getUserId(), matter.getTreeId(), treeNodeInfo.getTreeNodeId(), ruleKey, matterValue);
        }

        return treeNodeInfo;
    }

}
