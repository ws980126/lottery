package smu.ws.lottery.domain.activity.service.stateflow;

import smu.ws.lottery.common.Constants;
import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.activity.repository.IActivityRepository;

import javax.annotation.Resource;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 19:57
 * @description 活动状态抽象类
 */
public abstract class AbstractState {
    @Resource
    protected IActivityRepository activityRepository;

    /**
     * 活动提审
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result arraignment(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 审核通过
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result checkPass(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 审核拒绝
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result checkRefuse(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 撤审撤销
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result checkRevoke(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 活动关闭
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result close(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 活动开启
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result open(Long activityId, Enum<Constants.ActivityState>currentState);
    /**
     * 活动执行
     * @param activityId 活动Id
     * @param currentState 当前状态
     * @return
     */
    public abstract Result doing(Long activityId, Enum<Constants.ActivityState>currentState);



}
