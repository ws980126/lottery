package smu.ws.lottery.domain.activity.model.req;

import javax.xml.crypto.Data;
import java.util.Date;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/17 16:15
 * @description 参与活动请求
 */
public class PartakeReq {
    /**
     * 用户ID
     */
    private String uId;
    /**
     * 活动ID
     */
    private Long activityId;
    /**
     * 活动领取时间
     */
    private Date parktakeDate;

    public PartakeReq(String uId, Long activityId) {
        this.uId = uId;
        this.activityId = activityId;
    }

    public PartakeReq(String uId, Long activityId, Date parktakeDate) {
        this.uId = uId;
        this.activityId = activityId;
        this.parktakeDate = parktakeDate;
    }

    public PartakeReq() {
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Date getParktakeDate() {
        return parktakeDate;
    }

    public void setParktakeDate(Date parktakeDate) {
        this.parktakeDate = parktakeDate;
    }
}
