package smu.ws.lottery.domain.strategy.repository;

import org.springframework.stereotype.Repository;
import smu.ws.lottery.domain.strategy.model.aggregates.StrategyRich;
import smu.ws.lottery.domain.strategy.model.vo.AwardBriefVO;

import java.util.List;


public interface IStrategyRepository {

    StrategyRich queryStrategyRich(Long strategyId);

    AwardBriefVO queryAwardInfo(String awardId);


    List<String> queryNostackStrategyAwardList(Long strategyId);

    /**
     * 扣减库存
     * @param strategyId 策略Id
     * @param awardId 奖品Id
     * @return
     */
    boolean deductStock(Long strategyId,String awardId);

}
