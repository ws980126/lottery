package smu.ws.lottery.domain.award.service.goods.impl;

import org.springframework.stereotype.Component;
import smu.ws.lottery.common.Constants;
import smu.ws.lottery.domain.award.model.req.GoodReq;
import smu.ws.lottery.domain.award.model.res.DistributionRes;
import smu.ws.lottery.domain.award.service.goods.DistributionBase;
import smu.ws.lottery.domain.award.service.goods.IDistributionGoods;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/7 10:52
 * @description 实物发货类商品
 */
@Component
public class PhysicalGoods extends DistributionBase implements IDistributionGoods {
    @Override
    public DistributionRes doDistribution(GoodReq req) {
        // 模拟调用实物发奖
        logger.info("模拟调用实物发奖 uId：{} awardContent：{}", req.getuId(), req.getAwardContent());

        // 更新用户领奖结果
        super.updateUserAwardState(req.getuId(), req.getOrderId(), req.getAwardId(), Constants.GrantState.COMPLETE.getCode());

        return new DistributionRes(req.getuId(), Constants.AwardState.SUCCESS.getCode(), Constants.AwardState.SUCCESS.getInfo());
    }

    @Override
    public Integer getDistributionGoodsName() {
        return Constants.AwardType.PhysicalGoods.getCode();
    }
}
