package smu.ws.lottery.domain.activity.model.vo;

import java.util.Date;
import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 15:18
 * @description 策略信息配置
 */
public class StrategyVo {
    /**
     * 策略Id
     */
    private Long strategyId;
    /**
     * 策略描述
     */
    private String strategyDesc;

    /**
     * 策略方式1：单项概率， 2：总体概率
     */
    private Integer strategyMode;

    /**
     * 发放奖品方式1即时 2定时（含活动结束） 3人工
     */
    private Integer grantType;

    /**
     * 发放奖品时间
     */
    private Date grantDate;

    /**
     * 扩展信息
     */
    private String exInfo;

    /**
     * 策略详情配置
     */
    private List<StrategyDetailVo> strategyDetailList;

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public String getStrategyDesc() {
        return strategyDesc;
    }

    public void setStrategyDesc(String strategyDesc) {
        this.strategyDesc = strategyDesc;
    }

    public Integer getStrategyMode() {
        return strategyMode;
    }

    public void setStrategyMode(Integer strategyMode) {
        this.strategyMode = strategyMode;
    }

    public Integer getGrantType() {
        return grantType;
    }

    public void setGrantType(Integer grantType) {
        this.grantType = grantType;
    }

    public Date getGrantDate() {
        return grantDate;
    }

    public void setGrantDate(Date grantDate) {
        this.grantDate = grantDate;
    }

    public String getExInfo() {
        return exInfo;
    }

    public void setExInfo(String exInfo) {
        this.exInfo = exInfo;
    }

    public List<StrategyDetailVo> getStrategyDetailList() {
        return strategyDetailList;
    }

    public void setStrategyDetailList(List<StrategyDetailVo> strategyDetailList) {
        this.strategyDetailList = strategyDetailList;
    }

    @Override
    public String toString() {
        return "StrategyVo{" +
                "strategyId=" + strategyId +
                ", strategyDesc='" + strategyDesc + '\'' +
                ", strategyMode=" + strategyMode +
                ", grantType=" + grantType +
                ", grantDate=" + grantDate +
                ", exInfo='" + exInfo + '\'' +
                ", strategyDetailList=" + strategyDetailList +
                '}';
    }
}
