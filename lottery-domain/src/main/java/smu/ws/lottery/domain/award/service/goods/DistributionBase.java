package smu.ws.lottery.domain.award.service.goods;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smu.ws.lottery.domain.award.repository.IAwardRepository;
import javax.annotation.Resource;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/6 21:27
 * @description 配送货物基础共用类
 */
public class DistributionBase {
    protected Logger logger = LoggerFactory.getLogger(DistributionBase.class);

    @Resource
    private IAwardRepository awardRepository;

    protected void updateUserAwardState(String uId,Long orderId,String awardId,Integer grantState){
        awardRepository.updateUserAwardState(uId, orderId, awardId, grantState);
        logger.info("TODO,期添加更新分库分表中，用户个人的抽奖记录表中奖品发奖状态 uId: {}",uId);
    }
}
