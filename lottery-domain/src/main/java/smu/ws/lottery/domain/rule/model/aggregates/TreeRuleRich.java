package smu.ws.lottery.domain.rule.model.aggregates;

import smu.ws.lottery.domain.rule.model.vo.TreeNodeVo;
import smu.ws.lottery.domain.rule.model.vo.TreeRootVo;

import java.util.Map;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/21 18:53
 * @description 规则树集合
 */
public class TreeRuleRich {
    /**
     * 树根信息
     */
    private TreeRootVo treeRoot;

    /**
     * 树节点 -> 子节点
     */
    private Map<Long, TreeNodeVo> treeNodeMap;

    public TreeRootVo getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(TreeRootVo treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Map<Long, TreeNodeVo> getTreeNodeMap() {
        return treeNodeMap;
    }

    public void setTreeNodeMap(Map<Long, TreeNodeVo> treeNodeMap) {
        this.treeNodeMap = treeNodeMap;
    }
}
