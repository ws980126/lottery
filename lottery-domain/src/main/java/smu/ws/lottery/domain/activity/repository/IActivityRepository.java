package smu.ws.lottery.domain.activity.repository;

import smu.ws.lottery.common.Constants;
import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.activity.model.aggregates.ActivityInfoLimitPageRich;
import smu.ws.lottery.domain.activity.model.req.ActivityInfoLimitPageReq;
import smu.ws.lottery.domain.activity.model.req.PartakeReq;
import smu.ws.lottery.domain.activity.model.res.StockResult;
import smu.ws.lottery.domain.activity.model.vo.*;

import java.util.Date;
import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 16:40
 * @description 活动仓库服务（活动表，奖品表，策略表，策略明细表）
 */
public interface IActivityRepository {
    /**
     * 添加活动配置
     */
    void addActivity(ActivityVo activity);

    /**
     * 添加奖品配置集合
     */
    void addAward(List<AwardVo> awardList);

    /**
     * 添加策略配置
     */
    void addStrategy(StrategyVo strategy);

    /**
     * 添加策略明细配置
     */
    void addStrategyDetailList(List<StrategyDetailVo> IStrategyDetailList);

    /**
     * 变更活动状态
     * @param activityId 活动Id
     * @param beforeState 修改前活动状态
     * @param afterState 修改后活动状态
     * @return
     */

    boolean alterStatus(Long activityId, Enum<Constants.ActivityState> beforeState,Enum<Constants.ActivityState> afterState);

    /**
     *  查询活动账单信息【库存，状态，日期，个人参与次数】
     * @param req
     * @return
     */
    ActivityBillVo queryActivityBill(PartakeReq req);

    /**
     * 扣减活动库存
     * @param activityId   活动ID
     * @return      扣减结果
     */
    int subtractionActivityStock(Long activityId);

    List<ActivityVo> scanTODOAcyivityList(Long id);

    /**
     *
     * @param uId 用户ID
     * @param activityId 活动Id
     * @param stockCount 活动库存
     * @return 扣减结果
     */
    StockResult subtractionActivityStockByRedis(String uId, Long activityId, Integer stockCount, Date endDateTIme);

    /**
     *  恢复活动库存，通过Redis【如果遇到异常，则需要进行缓存库存恢复，只保证不超卖的特性，所以不保证一定能恢复占用库存，另外最终可以由任务金星补偿库存】
     * @param activityId 活动Id
     * @param stockKey 分布式KEY 用于清理
     * @param code 状态码
     */
    void recoverActivityCacheStockByRedis(Long activityId, String stockKey, String code);

    ActivityInfoLimitPageRich queryActivityInfoLimitPage(ActivityInfoLimitPageReq req);
}
