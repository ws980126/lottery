package smu.ws.lottery.domain.activity.model.req;

import smu.ws.lottery.domain.activity.model.aggregates.ActivityConfigRich;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 16:24
 * @description
 */
public class ActivityConfigReq {

    /**
     * 活动Id
     */
    private Long acticityId;

    /**
     * 活动配置信息
     */
    private ActivityConfigRich activityConfigRich;

    public ActivityConfigReq() {
    }

    public ActivityConfigReq(Long acticityId, ActivityConfigRich activityConfigRich) {
        this.acticityId = acticityId;
        this.activityConfigRich = activityConfigRich;
    }

    public Long getActicityId() {
        return acticityId;
    }

    public void setActicityId(Long acticityId) {
        this.acticityId = acticityId;
    }

    public ActivityConfigRich getActivityConfigRich() {
        return activityConfigRich;
    }

    public void setActivityConfigRich(ActivityConfigRich activityConfigRich) {
        this.activityConfigRich = activityConfigRich;
    }
}
