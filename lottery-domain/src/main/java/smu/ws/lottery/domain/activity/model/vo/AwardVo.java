package smu.ws.lottery.domain.activity.model.vo;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 15:49
 * @description  奖品信息配置
 */
public class AwardVo {
    /**
     * 奖品Id
     */
    private String awardId;

    /**
     * 奖品类型（1文字描述 2兑换码 3优惠券 4实物奖品）
     */
    private Integer awardType;

    /**
     * 奖品名称
     */
    private String awardName;

    /**
     * 奖品内容[描述，奖品码，sku]
     */

    private String awardContent;

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }

    public Integer getAwardType() {
        return awardType;
    }

    public void setAwardType(Integer awardType) {
        this.awardType = awardType;
    }

    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public String getAwardContent() {
        return awardContent;
    }

    public void setAwardContent(String awardContent) {
        this.awardContent = awardContent;
    }


    @Override
    public String toString() {
        return "AwardVo{" +
                "awardId='" + awardId + '\'' +
                ", awardType=" + awardType +
                ", awardName='" + awardName + '\'' +
                ", awardContent='" + awardContent + '\'' +
                '}';
    }
}
