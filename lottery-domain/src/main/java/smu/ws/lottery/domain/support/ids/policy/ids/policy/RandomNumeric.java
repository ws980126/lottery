package smu.ws.lottery.domain.support.ids.policy.ids.policy;


import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import smu.ws.lottery.domain.support.ids.IIdGenerator;


@Component
public class RandomNumeric implements IIdGenerator {

    @Override
    public long nextId() {
        return Long.parseLong(RandomStringUtils.randomNumeric(11));
    }

}
