package smu.ws.lottery.domain.activity.service.partake;

import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.activity.model.req.PartakeReq;
import smu.ws.lottery.domain.activity.model.res.PartakeResult;
import smu.ws.lottery.domain.activity.model.vo.ActivityPartakeRecordVO;
import smu.ws.lottery.domain.activity.model.vo.DrawOrderVO;
import smu.ws.lottery.domain.activity.model.vo.InvoiceVO;

import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 19:54
 * @description 抽奖活动参与接口
 */
public interface IActivityPartake {

    /**
     * 参与活动
     * @param req 入参
     * @return 领取结果
     */
    PartakeResult doPartake(PartakeReq req);

    /**
     *保存奖品单
     * @param buildDrawOrderVO 奖品单
     */
    Result recordDrawOrder(DrawOrderVO buildDrawOrderVO);


    /**
     * 更新发货单MQ状态
     * @param getuId 用户ID
     * @param orderId 订单ID
     * @param code MQ发送状态
     */
    void updateInvoiceMqState(String getuId, Long orderId, Integer code);


    /***
     *  扫描发货单MQ状态，把未发送MQ地单子扫描出来 做补偿
     * @param dbCount 指定分库
     * @param i 指定分表
     * @return 发货单
     */
    List<InvoiceVO> scanInvoiceMaState(int dbCount, int i);

    /**
     *
     * @param activityPartakeRecordVO 活动领取记录
     */
    void updateActivityStock(ActivityPartakeRecordVO activityPartakeRecordVO);

    Result lockTackActivity(String getuId, Long activityId, Long takeId);
}
