package smu.ws.lottery.domain.rule.service.logic.impl;

import org.springframework.stereotype.Component;
import smu.ws.lottery.domain.rule.model.req.DecisionMatterReq;
import smu.ws.lottery.domain.rule.service.logic.BaseLogic;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/21 20:00
 * @description
 */
@Component
public class UserAgeFilter extends BaseLogic {
    @Override
    public String matterValue(DecisionMatterReq decisionMatter) {
        return decisionMatter.getValMap().get("age").toString();
    }
}
