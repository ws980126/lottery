package smu.ws.lottery.domain.activity.service.partake;

import smu.ws.lottery.common.Constants;
import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.activity.model.req.PartakeReq;
import smu.ws.lottery.domain.activity.model.res.PartakeResult;
import smu.ws.lottery.domain.activity.model.res.StockResult;
import smu.ws.lottery.domain.activity.model.vo.ActivityBillVo;
import smu.ws.lottery.domain.activity.model.vo.UserTakeActivityVo;
import smu.ws.lottery.domain.support.ids.IIdGenerator;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/17 19:24
 * @description 模板模式处理活动流程
 */
public abstract class BaseActivityPartake extends ActivityPartakeSupport implements IActivityPartake{

    @Resource
    private Map<Constants.Ids, IIdGenerator> idGeneratorMap;


    @Override
    public PartakeResult doPartake(PartakeReq req) {

        //1、查询是否存在未执行抽奖领取活动单，【user_take_activity 存在 state = 0，领取了但抽奖过程失败的，可以直接返回领取结果继续抽奖】
        UserTakeActivityVo userTakeActivityVo = this.queryNoConsumedTakeActivityOrder(req.getActivityId(),req.getuId());
        if (null != userTakeActivityVo) {
            return buildPartakeResult(userTakeActivityVo.getStrategyId(),userTakeActivityVo.getTakeId(),Constants.ResponseCode.NOT_CONSUMED_TAKE);
        }
        //2、查询活动账单
        ActivityBillVo activityBillVo = super.queryActivityBill(req);
        //3、活动信息校验处理【活动库存，状态，日期，个人参与次数】
        Result checkResult = this.checkActivityBill(req,activityBillVo);
        if (!Constants.ResponseCode.SUCCESS.getCode().equals(checkResult.getCode())) {
            return new PartakeResult(checkResult.getCode(),checkResult.getInfo());
        }

        //4、扣减活动库存,通过Redis【活动库存扣减编号，作为领取的Key，缩小颗粒度】 Begin
        //Result subtractionActivityStockResult = this.subtractionActivityStock(req);
        StockResult subtractionActivityStockResult = this.subtractionActivityStockByRedis(req.getuId(),req.getActivityId(),activityBillVo.getStockCount(),activityBillVo.getEndDateTime());
        if (!Constants.ResponseCode.SUCCESS.getCode().equals(subtractionActivityStockResult.getCode())) {
            this.recoverActivityCacheStockByRedis(req.getActivityId(),subtractionActivityStockResult.getStockKey(),subtractionActivityStockResult.getCode());
            return new PartakeResult(subtractionActivityStockResult.getCode(),subtractionActivityStockResult.getInfo());
        }

        //5、插入领取活动信息【个人用户活动信息写入到用户表】
        long takeId = idGeneratorMap.get(Constants.Ids.SnowFlake).nextId();
        Result grabResult = this.grabActivity(req,activityBillVo,takeId);
        if (!Constants.ResponseCode.SUCCESS.getCode().equals(grabResult.getCode())) {
            this.recoverActivityCacheStockByRedis(req.getActivityId(),subtractionActivityStockResult.getStockKey(),Constants.ResponseCode.SUCCESS.getCode());
            return new PartakeResult(grabResult.getCode(),grabResult.getInfo());
        }

        //6.扣减活动库存 通过Redis End
        this.recoverActivityCacheStockByRedis(req.getActivityId(),subtractionActivityStockResult.getStockKey(),Constants.ResponseCode.SUCCESS.getCode());
        //封装结果
        return buildPartakeResult(activityBillVo.getStrategyId(),takeId,activityBillVo.getStockCount(),subtractionActivityStockResult.getStockSurplusCount(),Constants.ResponseCode.SUCCESS);
    }

    /**
     *
     * @param activityId 活动Id
     * @param stockKey 分布式 Key 用于清理
     * @param code 状态码
     */
    protected abstract void recoverActivityCacheStockByRedis(Long activityId, String stockKey, String code);


    /**
     *  封装结果【返回的策略ID,用于继续完成抽奖步骤】
     * @param strategyId 策略Id
     * @param takeId 领取ID
     * @param code 状态码
     * @return
     */

    private PartakeResult buildPartakeResult(Long strategyId, Long takeId,Constants.ResponseCode code) {
        PartakeResult partakeResult = new PartakeResult(code.getCode(), code.getInfo());
        partakeResult.setStrategyId(strategyId);
        partakeResult.setTakeId(takeId);
        return partakeResult;
    }

    private PartakeResult buildPartakeResult(Long strategyId, Long takeId,Integer stockCount,Integer stockSurplusCount,Constants.ResponseCode code) {
        PartakeResult partakeResult = new PartakeResult(code.getCode(), code.getInfo());
        partakeResult.setStrategyId(strategyId);
        partakeResult.setTakeId(takeId);
        partakeResult.setStockCount(stockCount);
        partakeResult.setStockSurplusCount(stockSurplusCount);
        return partakeResult;
    }

    /**
     * 查询是否存在未执行抽奖领取活动单【user_take_activity 存在 state = 0，领取了但抽奖过程失败的，可以直接返回领取结果继续抽奖】
     * @param activityId 活动ID
     * @param uId 用户ID
     * @return 领取单
     */
    protected abstract UserTakeActivityVo queryNoConsumedTakeActivityOrder(Long activityId, String uId);


    /**
     * 扣减活动库存
     *
     * @param req 参与活动请求
     * @return 扣减结果
     */
    protected abstract Result subtractionActivityStock(PartakeReq req);

    /**
     *
     * @param uId 用户Id
     * @param activityId 活动号
     * @param stockCount 总库存
     * @return 扣减结果
     */
    protected abstract StockResult subtractionActivityStockByRedis(String uId, Long activityId, Integer stockCount, Date endDateTime);

    /**
     * 领取活动
     *
     * @param partake 参与活动请求
     * @param bill    活动账单
     * @return 领取结果
     */

    protected abstract Result grabActivity(PartakeReq partake, ActivityBillVo bill,Long takeId);

    /**
     * 活动信息校验处理，把活动库存、状态、日期、个人参与次数
     *
     * @param partake 参与活动请求
     * @param bill    活动账单
     * @return 校验结果
     */
    protected abstract Result checkActivityBill(PartakeReq partake, ActivityBillVo bill);
}
