package smu.ws.lottery.domain.activity.model.aggregates;

import smu.ws.lottery.domain.activity.model.vo.ActivityVo;
import smu.ws.lottery.domain.activity.model.vo.AwardVo;
import smu.ws.lottery.domain.activity.model.vo.StrategyVo;

import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 14:53
 * @description 活动配置聚合信息
 */
public class ActivityConfigRich {
    /**
     * 活动配置
     */
    private ActivityVo activity;

    /**
     * 策略配置（含策略明细）
     */
    private StrategyVo strategy;

    /**
     * 奖品配置
     */

    private List<AwardVo> awardList;

    public ActivityConfigRich(ActivityVo activity, StrategyVo strategy, List<AwardVo> awardList) {
        this.activity = activity;
        this.strategy = strategy;
        this.awardList = awardList;
    }

    public ActivityConfigRich() {
    }

    public ActivityVo getActivity() {
        return activity;
    }

    public void setActivity(ActivityVo activity) {
        this.activity = activity;
    }

    public StrategyVo getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyVo strategy) {
        this.strategy = strategy;
    }

    public List<AwardVo> getAwardList() {
        return awardList;
    }

    public void setAwardVoList(List<AwardVo> awardList) {
        this.awardList = awardList;
    }
}
