package smu.ws.lottery.domain.activity.service.deploy.impl;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import smu.ws.lottery.domain.activity.model.aggregates.ActivityConfigRich;
import smu.ws.lottery.domain.activity.model.aggregates.ActivityInfoLimitPageRich;
import smu.ws.lottery.domain.activity.model.req.ActivityConfigReq;
import smu.ws.lottery.domain.activity.model.req.ActivityInfoLimitPageReq;
import smu.ws.lottery.domain.activity.model.vo.ActivityVo;
import smu.ws.lottery.domain.activity.model.vo.AwardVo;
import smu.ws.lottery.domain.activity.model.vo.StrategyDetailVo;
import smu.ws.lottery.domain.activity.model.vo.StrategyVo;
import smu.ws.lottery.domain.activity.repository.IActivityRepository;
import smu.ws.lottery.domain.activity.service.deploy.IActivityDeploy;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/8 18:57
 * @description 部署活动配置服务
 */

@Service
public class ActivityDeployImpl implements IActivityDeploy {

    private Logger logger = LoggerFactory.getLogger(ActivityDeployImpl.class) ;

    @Resource
    private IActivityRepository activityRepository;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createActivity(ActivityConfigReq req) {
        logger.info("创建活动配置开始，activityId: {}",req.getActicityId());
        ActivityConfigRich activityConfigRich = req.getActivityConfigRich();
        try {
            //创建活动配置
            ActivityVo activity = activityConfigRich.getActivity();
            activityRepository.addActivity(activity);

            //添加奖品配置
            List<AwardVo> awardList = activityConfigRich.getAwardList();
            activityRepository.addAward(awardList);

            //添加策略配置
            StrategyVo strategy = activityConfigRich.getStrategy();
            activityRepository.addStrategy(strategy);

            //添加策略明细配置
            List<StrategyDetailVo> strategyDetailList = activityConfigRich.getStrategy().getStrategyDetailList();
            activityRepository.addStrategyDetailList(strategyDetailList);

            logger.info("创建活动配置完成：acticityId {}",req.getActicityId());

        }catch (DuplicateKeyException e) {
            logger.error("创建活动配置失败，唯一索引冲突 activityId: {} reqJson; {}",req.getActicityId(), JSON.toJSONString(req),e);
            throw e;
        }

    }

    @Override
    public void updateActivity(ActivityConfigReq req) {
        //TODO 非核心功能后续补充
    }

    @Override
    public List<ActivityVo> scanTODOActivityList(Long id) {
        return activityRepository.scanTODOAcyivityList(id);
    }

    @Override
    public ActivityInfoLimitPageRich queryActivityInfoLimitPage(ActivityInfoLimitPageReq req) {
        return activityRepository.queryActivityInfoLimitPage(req);
    }
}
