package smu.ws.lottery.domain.rule.service.logic;

import smu.ws.lottery.domain.rule.model.req.DecisionMatterReq;
import smu.ws.lottery.domain.rule.model.vo.TreeNodeLineVo;

import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/21 16:41
 * @description 规则过滤器接口
 */
public interface LogicFilter {

    /**
     * 逻辑决策器
     * @param matterValue          决策值
     * @param treeNodeLineInfoList 决策节点
     * @return                     下一个节点Id
     */
    Long filter(String matterValue, List<TreeNodeLineVo> treeNodeLineInfoList);

    /**
     * 获取决策值
     *
     * @param decisionMatter 决策物料
     * @return               决策值
     */
    String matterValue(DecisionMatterReq decisionMatter);

}