package smu.ws.lottery.domain.award.service.factory;

import org.springframework.stereotype.Service;
import smu.ws.lottery.domain.award.service.goods.IDistributionGoods;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/7 11:17
 * @description 配送商品简单工厂，提供获取配送服务
 */
@Service
public class DistributionGoodsFactory extends GoodsConfig{
    public IDistributionGoods getDistributionGoodsService(Integer awardType){
        return goodsMap.get(awardType);
    }
}
