package smu.ws.lottery.domain.activity.model.aggregates;

import smu.ws.lottery.domain.activity.model.vo.ActivityVo;

import java.util.List;


public class ActivityInfoLimitPageRich {

    private Long count;
    private List<ActivityVo> activityVoList;

    public ActivityInfoLimitPageRich() {
    }

    public ActivityInfoLimitPageRich(Long count, List<ActivityVo> activityVoList) {
        this.count = count;
        this.activityVoList = activityVoList;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<ActivityVo> getActivityVOList() {
        return activityVoList;
    }

    public void setActivityVOList(List<ActivityVo> activityVOList) {
        this.activityVoList = activityVoList;
    }
}
