package smu.ws.lottery.domain.rule.service.engine.impl;

import org.springframework.stereotype.Service;
import smu.ws.lottery.domain.rule.model.aggregates.TreeRuleRich;
import smu.ws.lottery.domain.rule.model.req.DecisionMatterReq;
import smu.ws.lottery.domain.rule.model.res.EngineResult;
import smu.ws.lottery.domain.rule.model.vo.TreeNodeVo;
import smu.ws.lottery.domain.rule.repository.IRuleRepository;
import smu.ws.lottery.domain.rule.service.engine.EngineBase;

import javax.annotation.Resource;


@Service("ruleEngineHandle")
public class RuleEngineHandle extends EngineBase {

    @Resource
    private IRuleRepository ruleRepository;

    @Override
    public EngineResult process(DecisionMatterReq matter) {
        // 搭建决策规则树
        TreeRuleRich treeRuleRich = ruleRepository.queryTreeRuleRich(matter.getTreeId());
        if (null == treeRuleRich) {
            throw new RuntimeException("Tree Rule is null!");
        }

        // 决策节点
        TreeNodeVo treeNodeInfo = engineDecisionMaker(treeRuleRich, matter);

        // 决策结果
        return new EngineResult(matter.getUserId(), treeNodeInfo.getTreeId(), treeNodeInfo.getTreeNodeId(), treeNodeInfo.getNodeValue());
    }

}
