package smu.ws.lottery.domain.activity.service.partake;

import smu.ws.lottery.domain.activity.model.req.PartakeReq;
import smu.ws.lottery.domain.activity.model.vo.ActivityBillVo;
import smu.ws.lottery.domain.activity.repository.IActivityRepository;

import javax.annotation.Resource;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/17 16:52
 * @description
 */
public class ActivityPartakeSupport {

    @Resource
    protected IActivityRepository activityRepository;

    protected ActivityBillVo queryActivityBill(PartakeReq req){
        return activityRepository.queryActivityBill(req);
    }
}
