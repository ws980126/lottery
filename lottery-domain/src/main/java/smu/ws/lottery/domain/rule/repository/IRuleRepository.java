package smu.ws.lottery.domain.rule.repository;

import smu.ws.lottery.domain.rule.model.aggregates.TreeRuleRich;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/4/21 18:56
 * @description 规则信息仓储服务接口
 */
public interface IRuleRepository {

    /**
     * 查询规则决策树配置
     * @param treeId 决策树Id
     * @return 决策树配置
     */
    TreeRuleRich queryTreeRuleRich(Long treeId);
}
