package smu.ws.lottery.interfaces.assembler;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import smu.ws.lottery.domain.strategy.model.vo.DrawAwardVo;
import smu.ws.lottery.rpc.activity.booth.dto.AwardDTO;

/**
 * @description: 对象转换配置
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface AwardMapping extends IMapping<DrawAwardVo, AwardDTO> {

    @Mapping(target = "userId", source = "uId")
    @Override
    AwardDTO sourceToTarget(DrawAwardVo var1);

    @Override
    DrawAwardVo targetToSource(AwardDTO var1);

}
