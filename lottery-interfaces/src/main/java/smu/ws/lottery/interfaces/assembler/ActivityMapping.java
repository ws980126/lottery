package smu.ws.lottery.interfaces.assembler;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import smu.ws.lottery.domain.activity.model.vo.ActivityVo;
import smu.ws.lottery.rpc.activity.deploy.dto.ActivityDTO;

import java.util.List;

/**
 * @description: 活动对象转换配置
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface ActivityMapping extends IMapping<ActivityVo, ActivityDTO>{
    @Mapping(source = "createTIme", target = "createTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Override
    List<ActivityDTO> sourceToTarget(List<ActivityVo> var1);

}
