package smu.ws.lottery.interfaces.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import smu.ws.lottery.domain.strategy.model.vo.DrawAwardVo;
import smu.ws.lottery.rpc.activity.booth.dto.AwardDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-08T15:09:33+0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_371 (Oracle Corporation)"
)
@Component
public class AwardMappingImpl implements AwardMapping {

    @Override
    public List<AwardDTO> sourceToTarget(List<DrawAwardVo> var1) {
        if ( var1 == null ) {
            return null;
        }

        List<AwardDTO> list = new ArrayList<AwardDTO>( var1.size() );
        for ( DrawAwardVo drawAwardVo : var1 ) {
            list.add( sourceToTarget( drawAwardVo ) );
        }

        return list;
    }

    @Override
    public List<DrawAwardVo> targetToSource(List<AwardDTO> var1) {
        if ( var1 == null ) {
            return null;
        }

        List<DrawAwardVo> list = new ArrayList<DrawAwardVo>( var1.size() );
        for ( AwardDTO awardDTO : var1 ) {
            list.add( targetToSource( awardDTO ) );
        }

        return list;
    }

    @Override
    public List<AwardDTO> sourceToTarget(Stream<DrawAwardVo> stream) {
        if ( stream == null ) {
            return null;
        }

        return stream.map( drawAwardVo -> sourceToTarget( drawAwardVo ) )
        .collect( Collectors.toCollection( ArrayList<AwardDTO>::new ) );
    }

    @Override
    public List<DrawAwardVo> targetToSource(Stream<AwardDTO> stream) {
        if ( stream == null ) {
            return null;
        }

        return stream.map( awardDTO -> targetToSource( awardDTO ) )
        .collect( Collectors.toCollection( ArrayList<DrawAwardVo>::new ) );
    }

    @Override
    public AwardDTO sourceToTarget(DrawAwardVo var1) {
        if ( var1 == null ) {
            return null;
        }

        String userId = null;

        userId = var1.getuId();

        AwardDTO awardDTO = new AwardDTO( userId );

        awardDTO.setAwardId( var1.getAwardId() );
        awardDTO.setAwardType( var1.getAwardType() );
        awardDTO.setAwardName( var1.getAwardName() );
        awardDTO.setAwardContent( var1.getAwardContent() );
        awardDTO.setStrategyMode( var1.getStrategyMode() );
        awardDTO.setGrantType( var1.getGrantType() );
        awardDTO.setGrantDate( var1.getGrantDate() );

        return awardDTO;
    }

    @Override
    public DrawAwardVo targetToSource(AwardDTO var1) {
        if ( var1 == null ) {
            return null;
        }

        DrawAwardVo drawAwardVo = new DrawAwardVo();

        drawAwardVo.setAwardContent( var1.getAwardContent() );
        drawAwardVo.setAwardType( var1.getAwardType() );
        drawAwardVo.setAwardId( var1.getAwardId() );
        drawAwardVo.setAwardName( var1.getAwardName() );
        drawAwardVo.setStrategyMode( var1.getStrategyMode() );
        drawAwardVo.setGrantType( var1.getGrantType() );
        drawAwardVo.setGrantDate( var1.getGrantDate() );

        return drawAwardVo;
    }
}
