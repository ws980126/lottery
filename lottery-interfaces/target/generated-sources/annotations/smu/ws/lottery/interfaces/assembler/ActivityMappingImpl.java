package smu.ws.lottery.interfaces.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import smu.ws.lottery.domain.activity.model.vo.ActivityVo;
import smu.ws.lottery.rpc.activity.deploy.dto.ActivityDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-08T15:09:33+0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_371 (Oracle Corporation)"
)
@Component
public class ActivityMappingImpl implements ActivityMapping {

    @Override
    public ActivityDTO sourceToTarget(ActivityVo var1) {
        if ( var1 == null ) {
            return null;
        }

        ActivityDTO activityDTO = new ActivityDTO();

        activityDTO.setId( var1.getId() );
        activityDTO.setActivityId( var1.getActivityId() );
        activityDTO.setActivityName( var1.getActivityName() );
        activityDTO.setActivityDesc( var1.getActivityDesc() );
        activityDTO.setBeginDateTime( var1.getBeginDateTime() );
        activityDTO.setEndDateTime( var1.getEndDateTime() );
        activityDTO.setStockCount( var1.getStockCount() );
        activityDTO.setStockSurplusCount( var1.getStockSurplusCount() );
        activityDTO.setTakeCount( var1.getTakeCount() );
        activityDTO.setStrategyId( var1.getStrategyId() );
        activityDTO.setState( var1.getState() );
        activityDTO.setCreator( var1.getCreator() );
        activityDTO.setCreateTime( var1.getCreateTime() );
        activityDTO.setUpdateTime( var1.getUpdateTime() );

        return activityDTO;
    }

    @Override
    public ActivityVo targetToSource(ActivityDTO var1) {
        if ( var1 == null ) {
            return null;
        }

        ActivityVo activityVo = new ActivityVo();

        activityVo.setId( var1.getId() );
        activityVo.setActivityId( var1.getActivityId() );
        activityVo.setActivityName( var1.getActivityName() );
        activityVo.setActivityDesc( var1.getActivityDesc() );
        activityVo.setBeginDateTime( var1.getBeginDateTime() );
        activityVo.setEndDateTime( var1.getEndDateTime() );
        activityVo.setStockCount( var1.getStockCount() );
        activityVo.setTakeCount( var1.getTakeCount() );
        activityVo.setStrategyId( var1.getStrategyId() );
        activityVo.setState( var1.getState() );
        activityVo.setCreator( var1.getCreator() );
        activityVo.setCreateTime( var1.getCreateTime() );
        activityVo.setUpdateTime( var1.getUpdateTime() );
        activityVo.setStockSurplusCount( var1.getStockSurplusCount() );

        return activityVo;
    }

    @Override
    public List<ActivityVo> targetToSource(List<ActivityDTO> var1) {
        if ( var1 == null ) {
            return null;
        }

        List<ActivityVo> list = new ArrayList<ActivityVo>( var1.size() );
        for ( ActivityDTO activityDTO : var1 ) {
            list.add( targetToSource( activityDTO ) );
        }

        return list;
    }

    @Override
    public List<ActivityDTO> sourceToTarget(Stream<ActivityVo> stream) {
        if ( stream == null ) {
            return null;
        }

        return stream.map( activityVo -> sourceToTarget( activityVo ) )
        .collect( Collectors.toCollection( ArrayList<ActivityDTO>::new ) );
    }

    @Override
    public List<ActivityVo> targetToSource(Stream<ActivityDTO> stream) {
        if ( stream == null ) {
            return null;
        }

        return stream.map( activityDTO -> targetToSource( activityDTO ) )
        .collect( Collectors.toCollection( ArrayList<ActivityVo>::new ) );
    }

    @Override
    public List<ActivityDTO> sourceToTarget(List<ActivityVo> var1) {
        if ( var1 == null ) {
            return null;
        }

        List<ActivityDTO> list = new ArrayList<ActivityDTO>( var1.size() );
        for ( ActivityVo activityVo : var1 ) {
            list.add( sourceToTarget( activityVo ) );
        }

        return list;
    }
}
