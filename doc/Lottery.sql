/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - lottery
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lottery` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lottery`;

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `activity_id` bigint(20) NOT NULL COMMENT '活动ID',
  `activity_name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '活动名称',
  `activity_desc` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '活动描述',
  `begin_date_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date_time` datetime DEFAULT NULL COMMENT '结束时间',
  `stock_count` int(11) DEFAULT NULL COMMENT '库存',
  `stock_surplus_count` int(11) DEFAULT NULL COMMENT '库存剩余',
  `take_count` int(11) DEFAULT NULL COMMENT '每人可参与次数',
  `strategy_id` bigint(11) DEFAULT NULL COMMENT '抽奖策略ID',
  `state` tinyint(2) DEFAULT NULL COMMENT '活动状态：1编辑、2提审、3撤审、4通过、5运行(审核通过后worker扫描状态)、6拒绝、7关闭、8开启',
  `creator` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_activity_id` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='活动配置';

/*Data for the table `activity` */

insert  into `activity`(`id`,`activity_id`,`activity_name`,`activity_desc`,`begin_date_time`,`end_date_time`,`stock_count`,`stock_surplus_count`,`take_count`,`strategy_id`,`state`,`creator`,`create_time`,`update_time`) values 
(1,100001,'活动名','测试活动','2021-10-01 00:00:00','2022-12-01 23:59:59',100,75,10,10001,7,'xiaofuge','2021-08-08 20:14:50','2021-08-08 20:14:50'),
(3,100002,'活动名02','测试活动','2021-10-01 00:00:00','2022-12-01 23:59:59',100,100,10,10001,7,'xiaofuge','2021-10-05 15:49:21','2021-10-05 15:49:21'),
(7,120981321,'测试活动',NULL,'2023-05-07 11:28:11','2023-12-01 11:28:11',100,80,10,10001,1,'xiaofuge','2023-05-07 11:28:12','2023-05-07 11:28:12');

/*Table structure for table `award` */

DROP TABLE IF EXISTS `award`;

CREATE TABLE `award` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `award_id` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '奖品ID',
  `award_type` tinyint(4) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_award_id` (`award_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='奖品配置';

/*Data for the table `award` */

insert  into `award`(`id`,`award_id`,`award_type`,`award_name`,`award_content`,`create_time`,`update_time`) values 
(1,'1',1,'IMac','Code','2021-08-15 15:38:05','2021-08-15 15:38:05'),
(2,'2',1,'iphone','Code','2021-08-15 15:38:05','2021-08-15 15:38:05'),
(3,'3',1,'ipad','Code','2021-08-15 15:38:05','2021-08-15 15:38:05'),
(4,'4',1,'AirPods','Code','2021-08-15 15:38:05','2021-08-15 15:38:05'),
(5,'5',1,'Book','Code','2021-08-15 15:38:05','2021-08-15 15:38:05'),
(21,'101',1,'电脑','请联系活动组织者 fustack','2023-05-07 11:28:18','2023-05-07 11:28:18'),
(22,'102',1,'手机','请联系活动组织者 fustack','2023-05-07 11:28:18','2023-05-07 11:28:18'),
(23,'103',1,'平板','请联系活动组织者 fustack','2023-05-07 11:28:18','2023-05-07 11:28:18'),
(24,'104',1,'耳机','请联系活动组织者 fustack','2023-05-07 11:28:18','2023-05-07 11:28:18'),
(25,'105',1,'数据线','请联系活动组织者 fustack','2023-05-07 11:28:18','2023-05-07 11:28:18');

/*Table structure for table `rule_tree` */

DROP TABLE IF EXISTS `rule_tree`;

CREATE TABLE `rule_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tree_name` varchar(64) DEFAULT NULL COMMENT '规则树Id',
  `tree_desc` varchar(128) DEFAULT NULL COMMENT '规则树描述',
  `tree_root_node_id` bigint(20) DEFAULT NULL COMMENT '规则树根ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2110081903 DEFAULT CHARSET=utf8;

/*Data for the table `rule_tree` */

insert  into `rule_tree`(`id`,`tree_name`,`tree_desc`,`tree_root_node_id`,`create_time`,`update_time`) values 
(2110081902,'抽奖活动规则树','用于决策不同用户可参与的活动',1,'2021-10-08 15:38:05','2021-10-08 15:38:05');

/*Table structure for table `rule_tree_node` */

DROP TABLE IF EXISTS `rule_tree_node`;

CREATE TABLE `rule_tree_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tree_id` int(2) DEFAULT NULL COMMENT '规则树ID',
  `node_type` int(2) DEFAULT NULL COMMENT '节点类型；1子叶、2果实',
  `node_value` varchar(32) DEFAULT NULL COMMENT '节点值[nodeType=2]；果实值',
  `rule_key` varchar(16) DEFAULT NULL COMMENT '规则Key',
  `rule_desc` varchar(32) DEFAULT NULL COMMENT '规则描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

/*Data for the table `rule_tree_node` */

insert  into `rule_tree_node`(`id`,`tree_id`,`node_type`,`node_value`,`rule_key`,`rule_desc`) values 
(1,2110081902,1,NULL,'userGender','用户性别[男/女]'),
(11,2110081902,1,NULL,'userAge','用户年龄'),
(12,2110081902,1,NULL,'userAge','用户年龄'),
(111,2110081902,2,'100001',NULL,NULL),
(112,2110081902,2,'100002',NULL,NULL),
(121,2110081902,2,'100003',NULL,NULL),
(122,2110081902,2,'100004',NULL,NULL);

/*Table structure for table `rule_tree_node_line` */

DROP TABLE IF EXISTS `rule_tree_node_line`;

CREATE TABLE `rule_tree_node_line` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tree_id` bigint(20) DEFAULT NULL COMMENT '规则树ID',
  `node_id_from` bigint(20) DEFAULT NULL COMMENT '节点From',
  `node_id_to` bigint(20) DEFAULT NULL COMMENT '节点To',
  `rule_limit_type` int(2) DEFAULT NULL COMMENT '限定类型；1:=;2:>;3:<;4:>=;5<=;6:enum[枚举范围];7:果实',
  `rule_limit_value` varchar(32) DEFAULT NULL COMMENT '限定值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `rule_tree_node_line` */

insert  into `rule_tree_node_line`(`id`,`tree_id`,`node_id_from`,`node_id_to`,`rule_limit_type`,`rule_limit_value`) values 
(1,2110081902,1,11,1,'man'),
(2,2110081902,1,12,1,'woman'),
(3,2110081902,11,111,3,'25'),
(4,2110081902,11,112,4,'25'),
(5,2110081902,12,121,3,'25'),
(6,2110081902,12,122,4,'25');

/*Table structure for table `strategy` */

DROP TABLE IF EXISTS `strategy`;

CREATE TABLE `strategy` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `strategy_id` bigint(11) NOT NULL COMMENT '策略ID',
  `strategy_desc` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '策略描述',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发放奖品时间',
  `ext_info` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '扩展信息',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `strategy_strategyId_uindex` (`strategy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='策略配置';

/*Data for the table `strategy` */

insert  into `strategy`(`id`,`strategy_id`,`strategy_desc`,`strategy_mode`,`grant_type`,`grant_date`,`ext_info`,`create_time`,`update_time`) values 
(1,10001,'test',2,1,NULL,'','2021-09-25 08:15:52','2021-09-25 08:15:52'),
(4,10002,'抽奖策略',1,1,'2023-05-07 11:28:11',NULL,'2023-05-07 11:28:18','2023-05-07 11:28:18');

/*Table structure for table `strategy_detail` */

DROP TABLE IF EXISTS `strategy_detail`;

CREATE TABLE `strategy_detail` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `strategy_id` bigint(11) NOT NULL COMMENT '策略ID',
  `award_id` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '奖品ID',
  `award_name` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '奖品描述',
  `award_count` int(11) DEFAULT NULL COMMENT '奖品库存',
  `award_surplus_count` int(11) DEFAULT '0' COMMENT '奖品剩余库存',
  `award_rate` decimal(5,2) DEFAULT NULL COMMENT '中奖概率',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='策略明细';

/*Data for the table `strategy_detail` */

insert  into `strategy_detail`(`id`,`strategy_id`,`award_id`,`award_name`,`award_count`,`award_surplus_count`,`award_rate`,`create_time`,`update_time`) values 
(1,10001,'1','IMac',10,0,0.05,'2021-08-15 15:38:05','2021-08-15 15:38:05'),
(2,10001,'2','iphone',20,14,0.15,'2021-08-15 15:38:05','2021-08-15 15:38:05'),
(3,10001,'3','ipad',50,36,0.20,'2021-08-15 15:38:05','2021-08-15 15:38:05'),
(4,10001,'4','AirPods',100,63,0.25,'2021-08-15 15:38:05','2021-08-15 15:38:05'),
(5,10001,'5','Book',500,377,0.35,'2021-08-15 15:38:05','2021-08-15 15:38:05'),
(16,10002,'101','一等奖',NULL,10,0.05,'2023-05-07 11:28:18','2023-05-07 11:28:18'),
(17,10002,'102','二等奖',NULL,20,0.15,'2023-05-07 11:28:18','2023-05-07 11:28:18'),
(18,10002,'103','三等奖',NULL,50,0.20,'2023-05-07 11:28:18','2023-05-07 11:28:18'),
(19,10002,'104','四等奖',NULL,100,0.25,'2023-05-07 11:28:18','2023-05-07 11:28:18'),
(20,10002,'104','五等奖',NULL,500,0.35,'2023-05-07 11:28:18','2023-05-07 11:28:18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
