/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - lottery_01
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lottery_01` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lottery_01`;

/*Table structure for table `user_strategy_export_000` */

DROP TABLE IF EXISTS `user_strategy_export_000`;

CREATE TABLE `user_strategy_export_000` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_000` */

/*Table structure for table `user_strategy_export_001` */

DROP TABLE IF EXISTS `user_strategy_export_001`;

CREATE TABLE `user_strategy_export_001` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_001` */

insert  into `user_strategy_export_001`(`id`,`u_id`,`activity_id`,`order_id`,`strategy_id`,`strategy_mode`,`grant_type`,`grant_date`,`grant_state`,`award_id`,`award_type`,`award_name`,`award_content`,`uuid`,`mq_state`,`create_time`,`update_time`) values 
(1,'xiaofuge',100001,1655088902702256128,10001,2,1,'2023-05-07 13:58:01',1,'4',1,'AirPods','Code','1655088902702256128',1,'2023-05-07 13:55:30','2023-05-07 13:58:01'),
(2,'xiaofuge',100001,1655104364806819840,10001,2,1,'2023-05-07 14:56:53',1,'4',1,'AirPods','Code','1655104364806819840',1,'2023-05-07 14:56:46','2023-05-07 14:56:53'),
(3,'xiaofuge',100001,1655104621389172736,10001,2,1,'2023-05-07 14:57:46',1,'4',1,'AirPods','Code','1655104621389172736',1,'2023-05-07 14:57:46','2023-05-07 14:57:46');

/*Table structure for table `user_strategy_export_002` */

DROP TABLE IF EXISTS `user_strategy_export_002`;

CREATE TABLE `user_strategy_export_002` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_002` */

/*Table structure for table `user_strategy_export_003` */

DROP TABLE IF EXISTS `user_strategy_export_003`;

CREATE TABLE `user_strategy_export_003` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_003` */

insert  into `user_strategy_export_003`(`id`,`u_id`,`activity_id`,`order_id`,`strategy_id`,`strategy_mode`,`grant_type`,`grant_date`,`grant_state`,`award_id`,`award_type`,`award_name`,`award_content`,`uuid`,`mq_state`,`create_time`,`update_time`) values 
(1,'Uhdgkw766120d',120405215,1443558966104850432,42480428672,1,1,'2021-09-30 20:50:52',1,'1',1,'IMac','????','1443558966104850432',NULL,'2021-09-30 20:50:52','2021-09-30 20:50:52'),
(12,'1JMr6yGh9J',100001,1664563041129353216,10001,2,1,'2023-06-02 17:22:09',1,'2',1,'iphone','Code','1664563041129353216',1,'2023-06-02 17:22:09','2023-06-02 17:22:09'),
(13,'1JMr6yGh9J',100001,1664608133055664128,10001,2,1,'2023-06-02 20:21:24',1,'3',1,'ipad','Code','1664608133055664128',1,'2023-06-02 20:21:20','2023-06-02 20:21:24'),
(14,'1JMr6yGh9J',100001,1664608151003090944,10001,2,1,'2023-06-02 20:21:27',1,'3',1,'ipad','Code','1664608151003090944',1,'2023-06-02 20:21:25','2023-06-02 20:21:27'),
(15,'1JMr6yGh9J',100001,1664608329491697664,10001,2,1,'2023-06-02 20:22:07',1,'5',1,'Book','Code','1664608329491697664',1,'2023-06-02 20:22:07','2023-06-02 20:22:07'),
(16,'1JMr6yGh9J',100001,1664608433514631168,10001,2,1,'2023-06-02 20:22:32',1,'5',1,'Book','Code','1664608433514631168',1,'2023-06-02 20:22:32','2023-06-02 20:22:32');

/*Table structure for table `user_strategy_export_004` */

DROP TABLE IF EXISTS `user_strategy_export_004`;

CREATE TABLE `user_strategy_export_004` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` int(11) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_004` */

/*Table structure for table `user_take_activity` */

DROP TABLE IF EXISTS `user_take_activity`;

CREATE TABLE `user_take_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `take_id` bigint(20) DEFAULT NULL COMMENT '活动领取ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `activity_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动名称',
  `take_date` datetime DEFAULT NULL COMMENT '活动领取时间',
  `take_count` int(11) DEFAULT NULL COMMENT '领取次数',
  `strategy_id` int(11) DEFAULT NULL COMMENT '抽奖策略ID',
  `state` tinyint(2) DEFAULT NULL COMMENT '活动单使用状态 0未使用、1已使用',
  `uuid` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE COMMENT '防重ID'
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户参与活动记录表';

/*Data for the table `user_take_activity` */

insert  into `user_take_activity`(`id`,`u_id`,`take_id`,`activity_id`,`activity_name`,`take_date`,`take_count`,`strategy_id`,`state`,`uuid`,`create_time`,`update_time`) values 
(16,'Uhdgkw766120d',1443899607431151616,100001,'test','2021-10-01 19:24:27',2,NULL,NULL,'Uhdgkw766120d_100001_2','2021-10-01 19:24:27','2021-10-01 19:24:27'),
(17,'Uhdgkw766120d',1443900230654394368,100001,'test','2021-10-01 19:26:56',3,NULL,NULL,'Uhdgkw766120d_100001_3','2021-10-01 19:26:56','2021-10-01 19:26:56'),
(41,'xiaofuge',1655088039833260032,100001,'活动名',NULL,1,10001,1,'xiaofuge_100001_1','2023-05-07 13:52:11','2023-05-07 13:52:11'),
(42,'xiaofuge',1655104156404436992,100001,'活动名',NULL,2,10001,1,'xiaofuge_100001_2','2023-05-07 14:55:57','2023-05-07 14:55:57'),
(43,'xiaofuge',1655104618478325760,100001,'活动名',NULL,3,10001,1,'xiaofuge_100001_3','2023-05-07 14:57:46','2023-05-07 14:57:46'),
(54,'1JMr6yGh9J',1664563039875256320,100001,'活动名',NULL,1,10001,1,'1JMr6yGh9J_100001_1','2023-06-02 17:22:09','2023-06-02 17:22:09'),
(55,'1JMr6yGh9J',1664608127582097408,100001,'活动名',NULL,2,10001,1,'1JMr6yGh9J_100001_2','2023-06-02 20:21:19','2023-06-02 20:21:19'),
(56,'1JMr6yGh9J',1664608149019185152,100001,'活动名',NULL,3,10001,1,'1JMr6yGh9J_100001_3','2023-06-02 20:21:24','2023-06-02 20:21:24'),
(57,'1JMr6yGh9J',1664608327352602624,100001,'活动名',NULL,4,10001,1,'1JMr6yGh9J_100001_4','2023-06-02 20:22:06','2023-06-02 20:22:06'),
(58,'1JMr6yGh9J',1664608432206008320,100001,'活动名',NULL,5,10001,1,'1JMr6yGh9J_100001_5','2023-06-02 20:22:31','2023-06-02 20:22:31');

/*Table structure for table `user_take_activity_count` */

DROP TABLE IF EXISTS `user_take_activity_count`;

CREATE TABLE `user_take_activity_count` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `total_count` int(11) DEFAULT NULL COMMENT '可领取次数',
  `left_count` int(11) DEFAULT NULL COMMENT '已领取次数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uId_activityId` (`u_id`,`activity_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='用户活动参与次数表';

/*Data for the table `user_take_activity_count` */

insert  into `user_take_activity_count`(`id`,`u_id`,`activity_id`,`total_count`,`left_count`,`create_time`,`update_time`) values 
(1,'Uhdgkw766120d',100001,10,6,'2021-10-01 15:29:27','2021-10-01 15:29:27'),
(8,'xiaofuge',100001,10,7,'2023-05-07 13:52:11','2023-05-07 13:52:11'),
(10,'1JMr6yGh9J',100001,10,5,'2023-06-02 17:22:09','2023-06-02 17:22:09');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
