/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - lottery_02
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lottery_02` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lottery_02`;

/*Table structure for table `user_strategy_export_000` */

DROP TABLE IF EXISTS `user_strategy_export_000`;

CREATE TABLE `user_strategy_export_000` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_000` */

insert  into `user_strategy_export_000`(`id`,`u_id`,`activity_id`,`order_id`,`strategy_id`,`strategy_mode`,`grant_type`,`grant_date`,`grant_state`,`award_id`,`award_type`,`award_name`,`award_content`,`uuid`,`mq_state`,`create_time`,`update_time`) values 
(5,'fustack',100001,1454313513303539712,10001,2,1,NULL,0,'4',1,'AirPods','Code','1454313490931122176',1,'2021-10-30 13:05:36','2021-10-30 13:05:45'),
(6,'fustack',100001,1454313878879076352,10001,2,1,'2021-10-30 13:07:52',1,'3',1,'ipad','Code','1454313878132490240',1,'2021-10-30 13:07:03','2021-10-30 13:07:52'),
(7,'fustack',100001,1454314085880561664,10001,2,1,'2021-10-30 13:07:57',1,'4',1,'AirPods','Code','1454314085456936960',1,'2021-10-30 13:07:52','2021-10-30 13:07:57'),
(8,'fustack',100001,1454314251442323456,10001,2,1,'2021-10-30 13:08:35',1,'4',1,'AirPods','Code','1454314250930618368',1,'2021-10-30 13:08:32','2021-10-30 13:08:35'),
(9,'fustack',100001,1454314395218870272,10001,2,1,'2021-10-30 13:09:11',1,'3',1,'ipad','Code','1454314394698776576',1,'2021-10-30 13:09:06','2021-10-30 13:09:11'),
(10,'2iqM6Btck-',100001,1664616910869909504,10001,2,1,'2023-06-02 20:56:14',1,'5',1,'Book','Code','1664616910869909504',1,'2023-06-02 20:56:13','2023-06-02 20:56:14'),
(11,'2iqM6Btck-',100001,1664617114453037056,10001,2,1,'2023-06-02 20:57:01',1,'3',1,'ipad','Code','1664617114453037056',1,'2023-06-02 20:57:01','2023-06-02 20:57:01'),
(12,'2iqM6Btck-',100001,1664617273270358016,10001,2,1,'2023-06-02 20:57:40',1,'3',1,'ipad','Code','1664617273270358016',1,'2023-06-02 20:57:39','2023-06-02 20:57:40'),
(13,'2iqM6Btck-',100001,1664855718265769984,10001,2,1,'2023-06-03 12:45:15',1,'5',1,'Book','Code','1664855718265769984',1,'2023-06-03 12:45:11','2023-06-03 12:45:16'),
(14,'2iqM6Btck-',100001,1664857420494688256,10001,2,1,'2023-06-03 12:51:55',1,'4',1,'AirPods','Code','1664857420494688256',1,'2023-06-03 12:51:55','2023-06-03 12:51:55'),
(15,'2iqM6IvPY4',100001,1664885554996727808,10001,2,1,'2023-06-03 14:43:43',1,'2',1,'iphone','Code','1664885554996727808',1,'2023-06-03 14:43:43','2023-06-03 14:43:43'),
(16,'2iqM6Btck-',100001,1668442062506315776,10001,2,1,'2023-06-13 10:16:01',1,'4',1,'AirPods','Code','1668442062506315776',1,'2023-06-13 10:16:00','2023-06-13 10:16:01'),
(17,'2iqM6Btck-',100001,1668442106622005248,10001,2,1,'2023-06-13 10:16:11',1,'5',1,'Book','Code','1668442106622005248',1,'2023-06-13 10:16:11','2023-06-13 10:16:11');

/*Table structure for table `user_strategy_export_001` */

DROP TABLE IF EXISTS `user_strategy_export_001`;

CREATE TABLE `user_strategy_export_001` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_001` */

/*Table structure for table `user_strategy_export_002` */

DROP TABLE IF EXISTS `user_strategy_export_002`;

CREATE TABLE `user_strategy_export_002` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_002` */

/*Table structure for table `user_strategy_export_003` */

DROP TABLE IF EXISTS `user_strategy_export_003`;

CREATE TABLE `user_strategy_export_003` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` tinyint(4) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `mq_state` tinyint(4) DEFAULT NULL COMMENT '消息发送状态（0未发送、1发送成功、2发送失败）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_003` */

/*Table structure for table `user_strategy_export_004` */

DROP TABLE IF EXISTS `user_strategy_export_004`;

CREATE TABLE `user_strategy_export_004` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `order_id` bigint(32) DEFAULT NULL COMMENT '订单ID',
  `strategy_id` bigint(20) DEFAULT NULL COMMENT '策略ID',
  `strategy_mode` tinyint(2) DEFAULT NULL COMMENT '策略方式（1:单项概率、2:总体概率）',
  `grant_type` tinyint(2) DEFAULT NULL COMMENT '发放奖品方式（1:即时、2:定时[含活动结束]、3:人工）',
  `grant_date` datetime DEFAULT NULL COMMENT '发奖时间',
  `grant_state` int(11) DEFAULT NULL COMMENT '发奖状态',
  `award_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发奖ID',
  `award_type` tinyint(2) DEFAULT NULL COMMENT '奖品类型（1:文字描述、2:兑换码、3:优惠券、4:实物奖品）',
  `award_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品名称',
  `award_content` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '奖品内容「文字描述、Key、码」',
  `uuid` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户策略计算结果表';

/*Data for the table `user_strategy_export_004` */

/*Table structure for table `user_take_activity` */

DROP TABLE IF EXISTS `user_take_activity`;

CREATE TABLE `user_take_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `take_id` bigint(20) DEFAULT NULL COMMENT '活动领取ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `activity_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动名称',
  `take_date` datetime DEFAULT NULL COMMENT '活动领取时间',
  `take_count` int(11) DEFAULT NULL COMMENT '领取次数',
  `strategy_id` int(11) DEFAULT NULL COMMENT '抽奖策略ID',
  `state` tinyint(2) DEFAULT NULL COMMENT '活动单使用状态 0未使用、1已使用',
  `uuid` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '防重ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uuid` (`uuid`) USING BTREE COMMENT '防重ID'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户参与活动记录表';

/*Data for the table `user_take_activity` */

insert  into `user_take_activity`(`id`,`u_id`,`take_id`,`activity_id`,`activity_name`,`take_date`,`take_count`,`strategy_id`,`state`,`uuid`,`create_time`,`update_time`) values 
(4,'fustack',1444537120189480960,100001,'???','2021-10-03 13:37:33',1,10001,1,'fustack_100001_1','2021-10-03 13:37:42','2021-10-03 13:37:42'),
(6,'fustack',1444539043961536512,100001,'???','2021-10-03 13:45:18',2,10001,1,'fustack_100001_2','2021-10-03 13:45:37','2021-10-03 13:45:37'),
(7,'fustack',1444540455500021760,100001,'???','2021-10-03 13:50:57',3,10001,1,'fustack_100001_3','2021-10-03 13:50:57','2021-10-03 13:50:57'),
(8,'fustack',1444541564645965824,100001,'???','2021-10-03 13:55:22',4,10001,1,'fustack_100001_4','2021-10-03 13:55:21','2021-10-03 13:55:21'),
(9,'fustack',1444820310565273600,100001,'???','2021-10-04 08:23:00',5,10001,1,'fustack_100001_5','2021-10-04 08:23:00','2021-10-04 08:23:00'),
(11,'fustack',1454313490931122176,100001,'活动名','2021-10-30 13:05:29',6,10001,1,'fustack_100001_6','2021-10-30 13:05:30','2021-10-30 13:05:30'),
(12,'fustack',1454313878132490240,100001,'活动名','2021-10-30 13:06:59',7,10001,1,'fustack_100001_7','2021-10-30 13:07:03','2021-10-30 13:07:03'),
(13,'fustack',1454314085456936960,100001,'活动名','2021-10-30 13:07:51',8,10001,1,'fustack_100001_8','2021-10-30 13:07:52','2021-10-30 13:07:52'),
(14,'fustack',1454314250930618368,100001,'活动名','2021-10-30 13:08:31',9,10001,1,'fustack_100001_9','2021-10-30 13:08:31','2021-10-30 13:08:31'),
(15,'fustack',1454314394698776576,100001,'活动名','2021-10-30 13:09:06',10,10001,1,'fustack_100001_10','2021-10-30 13:09:06','2021-10-30 13:09:06'),
(16,'2iqM6Btck-',1664616906142928896,100001,'活动名',NULL,1,10001,1,'2iqM6Btck-_100001_1','2023-06-02 20:56:12','2023-06-02 20:56:12'),
(17,'2iqM6Btck-',1664617112808869888,100001,'活动名',NULL,2,10001,1,'2iqM6Btck-_100001_2','2023-06-02 20:57:01','2023-06-02 20:57:01'),
(18,'2iqM6Btck-',1664617271429058560,100001,'活动名',NULL,3,10001,1,'2iqM6Btck-_100001_3','2023-06-02 20:57:39','2023-06-02 20:57:39'),
(19,'2iqM6Btck-',1664855371166142467,100001,'活动名',NULL,4,10001,1,'2iqM6Btck-_100001_4','2023-06-03 12:44:20','2023-06-03 12:44:20'),
(21,'2iqM6Btck-',1664857419068624896,100001,'活动名',NULL,5,10001,1,'2iqM6Btck-_100001_5','2023-06-03 12:51:54','2023-06-03 12:51:54'),
(22,'2iqM6IvPY4',1664885549967757312,100001,'活动名',NULL,1,10001,1,'2iqM6IvPY4_100001_1','2023-06-03 14:43:41','2023-06-03 14:43:41'),
(23,'2iqM6Btck-',1668442048975491072,100001,'活动名',NULL,6,10001,1,'2iqM6Btck-_100001_6','2023-06-13 10:15:57','2023-06-13 10:15:57'),
(25,'2iqM6Btck-',1668442101047775232,100001,'活动名',NULL,7,10001,1,'2iqM6Btck-_100001_7','2023-06-13 10:16:09','2023-06-13 10:16:09');

/*Table structure for table `user_take_activity_count` */

DROP TABLE IF EXISTS `user_take_activity_count`;

CREATE TABLE `user_take_activity_count` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `u_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户ID',
  `activity_id` bigint(20) DEFAULT NULL COMMENT '活动ID',
  `total_count` int(11) DEFAULT NULL COMMENT '可领取次数',
  `left_count` int(11) DEFAULT NULL COMMENT '已领取次数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uId_activityId` (`u_id`,`activity_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='用户活动参与次数表';

/*Data for the table `user_take_activity_count` */

insert  into `user_take_activity_count`(`id`,`u_id`,`activity_id`,`total_count`,`left_count`,`create_time`,`update_time`) values 
(1,'fustack',100001,10,0,'2021-10-03 13:37:42','2021-10-03 13:37:42'),
(2,'2iqM6Btck-',100001,10,3,'2023-06-02 20:56:12','2023-06-02 20:56:12'),
(3,'2iqM6IvPY4',100001,10,9,'2023-06-03 14:43:41','2023-06-03 14:43:41');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
