/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.19 : Database - xxl_job
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xxl_job` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `xxl_job`;

/*Table structure for table `xxl_job_group` */

DROP TABLE IF EXISTS `xxl_job_group`;

CREATE TABLE `xxl_job_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) NOT NULL COMMENT '执行器AppName',
  `title` varchar(12) NOT NULL COMMENT '执行器名称',
  `address_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '执行器地址类型：0=自动注册、1=手动录入',
  `address_list` text COMMENT '执行器地址列表，多地址逗号分隔',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_group` */

insert  into `xxl_job_group`(`id`,`app_name`,`title`,`address_type`,`address_list`,`update_time`) values 
(1,'xxl-job-executor-sample','示例执行器',0,NULL,'2023-06-13 15:02:33'),
(4,'lottery-job','抽奖系统任务调度',0,NULL,'2023-06-13 15:02:33');

/*Table structure for table `xxl_job_info` */

DROP TABLE IF EXISTS `xxl_job_info`;

CREATE TABLE `xxl_job_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_group` int(11) NOT NULL COMMENT '执行器主键ID',
  `job_desc` varchar(255) NOT NULL,
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `author` varchar(64) DEFAULT NULL COMMENT '作者',
  `alarm_email` varchar(255) DEFAULT NULL COMMENT '报警邮件',
  `schedule_type` varchar(50) NOT NULL DEFAULT 'NONE' COMMENT '调度类型',
  `schedule_conf` varchar(128) DEFAULT NULL COMMENT '调度配置，值含义取决于调度类型',
  `misfire_strategy` varchar(50) NOT NULL DEFAULT 'DO_NOTHING' COMMENT '调度过期策略',
  `executor_route_strategy` varchar(50) DEFAULT NULL COMMENT '执行器路由策略',
  `executor_handler` varchar(255) DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) DEFAULT NULL COMMENT '执行器任务参数',
  `executor_block_strategy` varchar(50) DEFAULT NULL COMMENT '阻塞处理策略',
  `executor_timeout` int(11) NOT NULL DEFAULT '0' COMMENT '任务执行超时时间，单位秒',
  `executor_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `glue_type` varchar(50) NOT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) DEFAULT NULL COMMENT 'GLUE备注',
  `glue_updatetime` datetime DEFAULT NULL COMMENT 'GLUE更新时间',
  `child_jobid` varchar(255) DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
  `trigger_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '调度状态：0-停止，1-运行',
  `trigger_last_time` bigint(13) NOT NULL DEFAULT '0' COMMENT '上次调度时间',
  `trigger_next_time` bigint(13) NOT NULL DEFAULT '0' COMMENT '下次调度时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_info` */

insert  into `xxl_job_info`(`id`,`job_group`,`job_desc`,`add_time`,`update_time`,`author`,`alarm_email`,`schedule_type`,`schedule_conf`,`misfire_strategy`,`executor_route_strategy`,`executor_handler`,`executor_param`,`executor_block_strategy`,`executor_timeout`,`executor_fail_retry_count`,`glue_type`,`glue_source`,`glue_remark`,`glue_updatetime`,`child_jobid`,`trigger_status`,`trigger_last_time`,`trigger_next_time`) values 
(1,1,'测试任务1','2018-11-03 22:21:31','2023-05-04 15:17:44','XXL','','CRON','0 0 0 * * ? *','DO_NOTHING','FIRST','demoJobHandler','','SERIAL_EXECUTION',0,0,'BEAN','','GLUE代码初始化','2018-11-03 22:21:31','',1,1685808000000,1686672000000),
(4,4,'活动状态扫描','2023-05-04 21:02:06','2023-05-04 21:10:14','ws','','CRON','0 1-2 * * * ?','DO_NOTHING','FIRST','lotteryActivityStateJobHandler','','SERIAL_EXECUTION',0,0,'BEAN','','GLUE代码初始化','2023-05-04 21:02:06','',1,1686639720000,1686643260000);

/*Table structure for table `xxl_job_lock` */

DROP TABLE IF EXISTS `xxl_job_lock`;

CREATE TABLE `xxl_job_lock` (
  `lock_name` varchar(50) NOT NULL COMMENT '锁名称',
  PRIMARY KEY (`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_lock` */

insert  into `xxl_job_lock`(`lock_name`) values 
('schedule_lock');

/*Table structure for table `xxl_job_log` */

DROP TABLE IF EXISTS `xxl_job_log`;

CREATE TABLE `xxl_job_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `job_group` int(11) NOT NULL COMMENT '执行器主键ID',
  `job_id` int(11) NOT NULL COMMENT '任务，主键ID',
  `executor_address` varchar(255) DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
  `executor_handler` varchar(255) DEFAULT NULL COMMENT '执行器任务handler',
  `executor_param` varchar(512) DEFAULT NULL COMMENT '执行器任务参数',
  `executor_sharding_param` varchar(20) DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
  `executor_fail_retry_count` int(11) NOT NULL DEFAULT '0' COMMENT '失败重试次数',
  `trigger_time` datetime DEFAULT NULL COMMENT '调度-时间',
  `trigger_code` int(11) NOT NULL COMMENT '调度-结果',
  `trigger_msg` text COMMENT '调度-日志',
  `handle_time` datetime DEFAULT NULL COMMENT '执行-时间',
  `handle_code` int(11) NOT NULL COMMENT '执行-状态',
  `handle_msg` text COMMENT '执行-日志',
  `alarm_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
  PRIMARY KEY (`id`),
  KEY `I_trigger_time` (`trigger_time`),
  KEY `I_handle_code` (`handle_code`)
) ENGINE=InnoDB AUTO_INCREMENT=445 DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_log` */

insert  into `xxl_job_log`(`id`,`job_group`,`job_id`,`executor_address`,`executor_handler`,`executor_param`,`executor_sharding_param`,`executor_fail_retry_count`,`trigger_time`,`trigger_code`,`trigger_msg`,`handle_time`,`handle_code`,`handle_msg`,`alarm_status`) values 
(371,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-05-30 20:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-05-30 20:01:04',200,'',0),
(372,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-05-30 20:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-05-30 20:02:00',200,'',0),
(373,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 11:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(374,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 11:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(375,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 12:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(376,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 12:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(377,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 13:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(378,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 13:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(379,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 14:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(380,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 14:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(381,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 15:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-05-31 15:01:06',200,'',0),
(382,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 15:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-05-31 15:02:00',200,'',0),
(383,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 16:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(384,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 16:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(385,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 17:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(386,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 17:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(387,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 18:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(388,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 18:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(389,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 19:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(390,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 19:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(391,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 20:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(392,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 20:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(393,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 21:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(394,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-05-31 21:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(395,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 17:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 17:01:03',200,'',0),
(396,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 17:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 17:02:00',200,'',0),
(397,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 18:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 18:01:02',200,'',0),
(398,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 18:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 18:02:01',200,'',0),
(399,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 19:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 19:01:00',200,'',0),
(400,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 19:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 19:02:00',200,'',0),
(401,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 20:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(402,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 20:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(403,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 21:01:03',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 21:01:27',200,'',0),
(404,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 21:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 21:02:01',200,'',0),
(405,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 22:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 22:01:09',200,'',0),
(406,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-01 22:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-01 22:02:01',200,'',0),
(407,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 10:01:01',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 10:01:02',200,'',0),
(408,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 10:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 10:02:00',200,'',0),
(409,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 11:01:01',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：500<br>msg：xxl-job remoting error(Read timed out), for url : http://222.197.103.71:9999/run','2023-06-02 11:01:32',200,'',2),
(410,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 11:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 11:02:01',200,'',0),
(411,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 12:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(412,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 12:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(413,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 13:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(414,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 13:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(415,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 14:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(416,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 14:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(417,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 15:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(418,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 15:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(419,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 16:01:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：500<br>msg：xxl-job remoting error(Read timed out), for url : http://222.197.103.71:9999/run','2023-06-02 16:09:04',200,'',2),
(420,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 16:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：500<br>msg：xxl-job remoting error(Read timed out), for url : http://222.197.103.71:9999/run','2023-06-02 16:09:04',200,'',2),
(421,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 17:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 17:01:01',200,'',0),
(422,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 17:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 17:02:00',200,'',0),
(423,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 18:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 18:01:02',200,'',0),
(424,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 18:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 18:02:00',200,'',0),
(425,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 19:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 19:01:00',200,'',0),
(426,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 19:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 19:02:00',200,'',0),
(427,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 20:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 20:01:00',200,'',0),
(428,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 20:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 20:02:00',200,'',0),
(429,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 21:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 21:01:02',200,'',0),
(430,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-02 21:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-02 21:02:02',200,'',0),
(431,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-03 13:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-03 13:01:01',200,'',0),
(432,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-03 13:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-03 13:02:00',200,'',0),
(433,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 10:01:02',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(434,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 10:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2),
(435,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 11:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 11:01:06',200,'',0),
(436,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 11:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 11:02:01',200,'',0),
(437,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 12:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 12:01:01',200,'',0),
(438,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 12:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 12:02:00',200,'',0),
(439,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 13:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 13:01:00',200,'',0),
(440,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 13:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 13:02:00',200,'',0),
(441,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 14:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 14:01:00',200,'',0),
(442,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 14:02:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 14:02:00',200,'',0),
(443,4,4,'http://222.197.103.71:9999/','lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 15:01:00',200,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：[http://222.197.103.71:9999/]<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>触发调度：<br>address：http://222.197.103.71:9999/<br>code：200<br>msg：null','2023-06-13 15:01:00',200,'',0),
(444,4,4,NULL,'lotteryActivityStateJobHandler','',NULL,0,'2023-06-13 15:02:00',500,'任务触发类型：Cron触发<br>调度机器：222.197.103.71<br>执行器-注册方式：自动注册<br>执行器-地址列表：null<br>路由策略：第一个<br>阻塞处理策略：单机串行<br>任务超时时间：0<br>失败重试次数：0<br><br><span style=\"color:#00c0ef;\" > >>>>>>>>>>>触发调度<<<<<<<<<<< </span><br>调度失败：执行器地址为空<br><br>',NULL,0,NULL,2);

/*Table structure for table `xxl_job_log_report` */

DROP TABLE IF EXISTS `xxl_job_log_report`;

CREATE TABLE `xxl_job_log_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trigger_day` datetime DEFAULT NULL COMMENT '调度-时间',
  `running_count` int(11) NOT NULL DEFAULT '0' COMMENT '运行中-日志数量',
  `suc_count` int(11) NOT NULL DEFAULT '0' COMMENT '执行成功-日志数量',
  `fail_count` int(11) NOT NULL DEFAULT '0' COMMENT '执行失败-日志数量',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_trigger_day` (`trigger_day`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_log_report` */

insert  into `xxl_job_log_report`(`id`,`trigger_day`,`running_count`,`suc_count`,`fail_count`,`update_time`) values 
(1,'2023-05-04 00:00:00',0,37,136,NULL),
(2,'2023-05-03 00:00:00',0,0,0,NULL),
(3,'2023-05-02 00:00:00',0,0,0,NULL),
(4,'2023-05-05 00:00:00',0,5,0,NULL),
(5,'2023-05-06 00:00:00',0,0,8,NULL),
(6,'2023-05-07 00:00:00',0,2,39,NULL),
(7,'2023-05-30 00:00:00',0,2,0,NULL),
(9,'2023-05-29 00:00:00',0,0,0,NULL),
(10,'2023-05-28 00:00:00',0,0,0,NULL),
(11,'2023-05-31 00:00:00',0,2,20,NULL),
(12,'2023-06-01 00:00:00',0,10,2,NULL),
(13,'2023-06-02 00:00:00',0,16,8,NULL),
(14,'2023-06-03 00:00:00',0,2,0,NULL),
(15,'2023-06-13 00:00:00',0,9,3,NULL),
(16,'2023-06-12 00:00:00',0,0,0,NULL),
(17,'2023-06-11 00:00:00',0,0,0,NULL);

/*Table structure for table `xxl_job_logglue` */

DROP TABLE IF EXISTS `xxl_job_logglue`;

CREATE TABLE `xxl_job_logglue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL COMMENT '任务，主键ID',
  `glue_type` varchar(50) DEFAULT NULL COMMENT 'GLUE类型',
  `glue_source` mediumtext COMMENT 'GLUE源代码',
  `glue_remark` varchar(128) NOT NULL COMMENT 'GLUE备注',
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_logglue` */

/*Table structure for table `xxl_job_registry` */

DROP TABLE IF EXISTS `xxl_job_registry`;

CREATE TABLE `xxl_job_registry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_group` varchar(50) NOT NULL,
  `registry_key` varchar(255) NOT NULL,
  `registry_value` varchar(255) NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `i_g_k_v` (`registry_group`,`registry_key`,`registry_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_registry` */

/*Table structure for table `xxl_job_user` */

DROP TABLE IF EXISTS `xxl_job_user`;

CREATE TABLE `xxl_job_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '账号',
  `password` varchar(50) NOT NULL COMMENT '密码',
  `role` tinyint(4) NOT NULL COMMENT '角色：0-普通用户、1-管理员',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `i_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `xxl_job_user` */

insert  into `xxl_job_user`(`id`,`username`,`password`,`role`,`permission`) values 
(1,'admin','e10adc3949ba59abbe56e057f20f883e',1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
