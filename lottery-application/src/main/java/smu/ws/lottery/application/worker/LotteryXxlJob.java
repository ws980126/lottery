package smu.ws.lottery.application.worker;

import cn.bugstack.middleware.db.router.strategy.IDBRouterStrategy;
import com.alibaba.fastjson.JSON;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import smu.ws.lottery.application.mq.producer.KafkaProducer;
import smu.ws.lottery.common.Constants;
import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.activity.model.vo.ActivityVo;
import smu.ws.lottery.domain.activity.model.vo.InvoiceVO;
import smu.ws.lottery.domain.activity.service.deploy.IActivityDeploy;
import smu.ws.lottery.domain.activity.service.partake.IActivityPartake;
import smu.ws.lottery.domain.activity.service.stateflow.IStateHandler;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author 王硕
 * @version 1.0
 * @date 2023/5/4 15:35
 * @description 抽奖业务，任务配置
 */
@Component
public class LotteryXxlJob {

    private Logger logger = LoggerFactory.getLogger(LotteryXxlJob.class);

    @Resource
    private IActivityPartake activityPartake;

    @Resource
    private IActivityDeploy activityDeploy;

    @Resource
    private IStateHandler stateHandler;

    @Resource
    private IDBRouterStrategy dbRouter;

    @Resource
    private KafkaProducer kafkaProducer;

    @XxlJob("lotteryActivityStateJobHandler")
    public void lotteryActivityJobHandler() throws Exception {
        logger.info("扫描活动状态 Begin");

        List<ActivityVo> activityVoList = activityDeploy.scanTODOActivityList(0L);
        if (activityVoList.isEmpty()) {
            logger.info("扫描活动状态 End 暂无符合需要扫描地活动列表");
            return;
        }
        while (!activityVoList.isEmpty()) {
            for (ActivityVo activityVo : activityVoList) {
                Integer state = activityVo.getState();
                switch (state) {
                    // 活动状态为审核通过，在临近活动开启时间前，审核活动为活动中。在使用活动的时候，需要依照活动状态核时间两个字段进行判断和使用。
                    case 4:
                        Result state4Result = stateHandler.doing(activityVo.getActivityId(), Constants.ActivityState.PASS);
                        logger.info("扫描活动状态为活动中 结果：{} activityId：{} activityName：{} creator：{}", JSON.toJSONString(state4Result), activityVo.getActivityId(), activityVo.getActivityName(), activityVo.getCreator());
                        break;

                    // 扫描时间已过期的活动，从活动中状态变更为关闭状态【这里也可以细化为2个任务来处理，也可以把时间判断放到数据库中操作】
                    case 5:
                        if (activityVo.getEndDateTime().before(new Date())) {
                            Result state5Result = stateHandler.close(activityVo.getActivityId(), Constants.ActivityState.DOING);
                            logger.info("扫描活动状态为关闭 结果：{} activityId：{} activityName：{} creator：{}", JSON.toJSONString(state5Result), activityVo.getActivityId(), activityVo.getActivityName(), activityVo.getCreator());
                        }
                        break;
                    default:
                        break;
                }
            }
            // 获取集合中最后一条记录，继续扫描后面10条记录
            ActivityVo activityVO = activityVoList.get(activityVoList.size() - 1);
            activityVoList = activityDeploy.scanTODOActivityList(activityVO.getId());
        }
        logger.info("扫描活动状态 End");
    }


    @XxlJob("lotteryOrderMQStateJobHandler")
    public void lotteryOrderMQStateJobHandler() throws Exception {
        String jobParm = XxlJobHelper.getJobParam();
        if (null == jobParm) {
            logger.info("扫描用户抽奖发放MQ状态[Table = 2*4] 错误 parm is null");
            return;
        }

        // 获取分布式任务配置参数信息 参数配置格式：1,2,3 也可以是指定扫描一个，也可以配置多个库，按照部署的任务集群进行数量配置，均摊分别扫描效率更高
        String[] parms = jobParm.split(".");
        logger.info("扫描用户抽奖奖品发放状态[Table = 2 * 4] 开始 parms： {}",JSON.toJSONString(parms));

        if (parms.length == 0) {
            logger.info("扫描用户抽奖发放状态[Table = 2 * 4] 结束 parms is null");
            return;
        }

        //获取分库分表配置下的分表数
        int tbCount = dbRouter.tbCount();

        //循环获取指定扫描库
        for (String param : parms) {
            //获取当前任务扫描的指定分库
            int dbCount = Integer.parseInt(param);

            //判断配置指定扫描库数，是否存在
            if (dbCount > dbRouter.dbCount()) {
                logger.info("扫描用户抽奖奖品发放MQ状态[Table = 2 * 4] 结束 dbCount not exist");
                continue;
            }

            //循环扫描对应表
            for (int i = 0; i < tbCount; i++) {
                //扫描库表数量
                List<InvoiceVO>invoiceVOList = activityPartake.scanInvoiceMaState(dbCount,i);
                logger.info("扫描用户抽奖奖品发放状态[Table = 2 * 4] 扫描库: {} 扫描表: {} 扫描数: {}",dbCount,i,invoiceVOList.size());

                //补偿MQ消息
                for (InvoiceVO invoiceVO : invoiceVOList) {
                    ListenableFuture<SendResult<String, Object>> future = kafkaProducer.sendLotteryInvoice(invoiceVO);
                    future.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                            //MQ消息发送失败，更新数据表 user_strategy_export.mq_state = 2[等待定时任务扫码补偿MQ消息]
                            activityPartake.updateInvoiceMqState(invoiceVO.getuId(),invoiceVO.getOrderId(),Constants.MQState.FAIL.getCode());
                        }

                        @Override
                        public void onSuccess(SendResult<String, Object> stringObjectSendResult) {
                            //MQ消息发送完成，更新数据库表user_strategy_export.mq_state = 1
                            activityPartake.updateInvoiceMqState(invoiceVO.getuId(),invoiceVO.getOrderId(),Constants.MQState.COMPLETE.getCode());
                        }
                    });
                }
            }
        }
        logger.info("扫描用户抽奖奖品发放MQ状态【table = 2 * 4】 完成 param: {}",JSON.toJSONString(parms));
    }
}
