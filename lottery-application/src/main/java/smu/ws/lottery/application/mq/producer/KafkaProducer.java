package smu.ws.lottery.application.mq.producer;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import smu.ws.lottery.domain.activity.model.vo.ActivityPartakeRecordVO;
import smu.ws.lottery.domain.activity.model.vo.InvoiceVO;

import javax.annotation.Resource;

/**
 * @description: 消息生产者
 */
@Component
public class KafkaProducer {

    private Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;

    public static final String TOPIC_INVOICE = "lottery_invoice01";
    public static final String TOPIC_ACTIVITY_PARTAKE = "lottery_invoice02";


    /**
     * 发送中奖物品发货单消息
     *
     * @param invoice 发货单
     */
    public ListenableFuture<SendResult<String, Object>> sendLotteryInvoice(InvoiceVO invoice) {
        String objJson = JSON.toJSONString(invoice);
        logger.info("发送MQ消息 topic：{} bizId：{} message：{}", TOPIC_INVOICE, invoice.getuId(), objJson);
        return kafkaTemplate.send(TOPIC_INVOICE, objJson);
    }

    /**
     * 发送领取活动记录MQ
     * @param activityPartakeRecord 领取活动记录
     */
    public ListenableFuture<SendResult<String, Object>>sendLotteryActivityPartakeRecord(ActivityPartakeRecordVO activityPartakeRecord) {
        String objJson = JSON.toJSONString(activityPartakeRecord);
        logger.info("发送MQ消息（领取活动记录） topic: {} bizId: {} message: {}",TOPIC_ACTIVITY_PARTAKE,activityPartakeRecord.getuId(),objJson);
        return kafkaTemplate.send(TOPIC_ACTIVITY_PARTAKE,objJson);
    }
}
