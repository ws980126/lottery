package smu.ws.lottery.application.process.draw.res;


import smu.ws.lottery.common.Result;
import smu.ws.lottery.domain.strategy.model.vo.DrawAwardVo;


public class DrawProcessResult extends Result {

    private DrawAwardVo drawAwardVO;

    public DrawProcessResult(String code, String info) {
        super(code, info);
    }

    public DrawProcessResult(String code, String info, DrawAwardVo drawAwardVO) {
        super(code, info);
        this.drawAwardVO = drawAwardVO;
    }

    public DrawAwardVo getDrawAwardVO() {
        return drawAwardVO;
    }

    public void setDrawAwardVO(DrawAwardVo drawAwardVO) {
        this.drawAwardVO = drawAwardVO;
    }

}
