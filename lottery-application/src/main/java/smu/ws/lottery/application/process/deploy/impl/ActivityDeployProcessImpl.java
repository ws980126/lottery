package smu.ws.lottery.application.process.deploy.impl;


import org.springframework.stereotype.Service;
import smu.ws.lottery.application.process.deploy.IActivityDeployProcess;
import smu.ws.lottery.domain.activity.model.aggregates.ActivityInfoLimitPageRich;
import smu.ws.lottery.domain.activity.model.req.ActivityInfoLimitPageReq;
import smu.ws.lottery.domain.activity.service.deploy.IActivityDeploy;

import javax.annotation.Resource;


@Service
public class ActivityDeployProcessImpl implements IActivityDeployProcess {

    @Resource
    private IActivityDeploy activityDeploy;

    @Override
    public ActivityInfoLimitPageRich queryActivityInfoLimitPage(ActivityInfoLimitPageReq req) {
        return activityDeploy.queryActivityInfoLimitPage(req);
    }
}
