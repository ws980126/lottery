package smu.ws.lottery.rpc.activity.booth;


import smu.ws.lottery.rpc.activity.booth.req.QuantificationDrawReq;
import smu.ws.lottery.rpc.activity.booth.res.DrawRes;
import smu.ws.lottery.rpc.activity.booth.req.DrawReq;


public interface ILotteryActivityBooth {

    /**
     * 指定活动抽奖
     * @param drawReq 请求参数
     * @return        抽奖结果
     */
    DrawRes doDraw(DrawReq drawReq);

    /**
     * 量化人群抽奖
     * @param quantificationDrawReq 请求参数
     * @return                      抽奖结果
     */
    DrawRes doQuantificationDraw(QuantificationDrawReq quantificationDrawReq);

}
