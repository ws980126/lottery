package smu.ws.lottery.infrastructure.dao;

import org.apache.ibatis.annotations.Mapper;
import smu.ws.lottery.infrastructure.po.Strategy;


@Mapper
public interface IStrategyDao {

    Strategy queryStrategy(Long strategyId);

    /**
     * 插入策略配置
     *
     * @param req 策略配置
     */
    void insert(Strategy req);

}
