package smu.ws.lottery.infrastructure.Repository;


import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import smu.ws.lottery.domain.award.repository.IAwardRepository;
import smu.ws.lottery.infrastructure.dao.IUserStrategyExportDao;
import smu.ws.lottery.infrastructure.po.UserStrategyExport;

import javax.annotation.Resource;


@Repository
public class AwardRepository implements IAwardRepository {

    @Resource
    private IUserStrategyExportDao userStrategyExportDao;

    @Override
    public void updateUserAwardState(String uId, Long orderId, String awardId, Integer grantState) {
        UserStrategyExport userStrategyExport = new UserStrategyExport();
        userStrategyExport.setuId(uId);
        userStrategyExport.setOrderId(orderId);
        userStrategyExport.setAwardId(awardId);
        userStrategyExport.setGrantState(grantState);
        userStrategyExportDao.updateUserAwardState(userStrategyExport);
    }
}
