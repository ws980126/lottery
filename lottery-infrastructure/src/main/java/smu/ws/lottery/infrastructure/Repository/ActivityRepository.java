package smu.ws.lottery.infrastructure.Repository;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import smu.ws.lottery.common.Constants;
import smu.ws.lottery.domain.activity.model.aggregates.ActivityInfoLimitPageRich;
import smu.ws.lottery.domain.activity.model.req.ActivityInfoLimitPageReq;
import smu.ws.lottery.domain.activity.model.req.PartakeReq;
import smu.ws.lottery.domain.activity.model.res.StockResult;
import smu.ws.lottery.domain.activity.model.vo.*;
import smu.ws.lottery.domain.activity.repository.IActivityRepository;
import smu.ws.lottery.infrastructure.dao.*;
import smu.ws.lottery.infrastructure.po.*;
import smu.ws.lottery.infrastructure.util.RedisUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository
public class ActivityRepository implements IActivityRepository {

    private Logger logger = LoggerFactory.getLogger(ActivityRepository.class);

    @Resource
    private IActivityDao activityDao;
    @Resource
    private IAwardDao awardDao;
    @Resource
    private IStrategyDao strategyDao;
    @Resource
    private IStrategyDetailDao strategyDetailDao;
    @Resource
    private IUserTakeActivityCountDao userTakeActivityCountDao;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public void addActivity(ActivityVo activity) {
        Activity req = new Activity();
        BeanUtils.copyProperties(activity, req);
        activityDao.insert(req);

        //设置活动库存KEY
        redisUtil.set(Constants.RedisKey.KEY_LOTTERY_ACTIVITY_STOCK_COUNT(activity.getActivityId()),0);
    }

    @Override
    public void addAward(List<AwardVo> awardList) {
        List<Award> req = new ArrayList<>();
        for (AwardVo awardVO : awardList) {
            Award award = new Award();
            BeanUtils.copyProperties(awardVO, award);
            req.add(award);
        }
        awardDao.insertList(req);
    }

    @Override
    public void addStrategy(StrategyVo strategy) {
        Strategy req = new Strategy();
        BeanUtils.copyProperties(strategy, req);
        strategyDao.insert(req);
    }

    @Override
    public void addStrategyDetailList(List<StrategyDetailVo> strategyDetailList) {
        List<StrategyDetail> req = new ArrayList<>();
        for (StrategyDetailVo strategyDetailVO : strategyDetailList) {
            StrategyDetail strategyDetail = new StrategyDetail();
            BeanUtils.copyProperties(strategyDetailVO, strategyDetail);
            req.add(strategyDetail);
        }
        strategyDetailDao.insertList(req);
    }

    @Override
    public boolean alterStatus(Long activityId, Enum<Constants.ActivityState> beforeState, Enum<Constants.ActivityState> afterState) {
        AlterStateVO alterStateVO = new AlterStateVO(activityId, ((Constants.ActivityState) beforeState).getCode(), ((Constants.ActivityState) afterState).getCode());
        int count = activityDao.alterState(alterStateVO);
        return 1 == count;
    }

    @Override
    public ActivityBillVo queryActivityBill(PartakeReq req) {
        //查询活动信息
        Activity activity = activityDao.queryActivityById(req.getActivityId());

        //从缓存中获取库存
        Object usedStockCountObj = redisUtil.get(Constants.RedisKey.KEY_LOTTERY_ACTIVITY_STOCK_COUNT(req.getActivityId()));

        //查询总次数和剩余次数
        UserTakeActivityCount userTakeActivityCountReq = new UserTakeActivityCount();
        userTakeActivityCountReq.setuId(req.getuId());
        userTakeActivityCountReq.setActivityId(req.getActivityId());
        UserTakeActivityCount userTakeActivityCount = userTakeActivityCountDao.queryUserTakeActivityCount(userTakeActivityCountReq);

        //封装结果信息
        // 封装结果信息
        ActivityBillVo activityBillVO = new ActivityBillVo();
        activityBillVO.setuId(req.getuId());
        activityBillVO.setActivityId(req.getActivityId());
        activityBillVO.setActivityName(activity.getActivityName());
        activityBillVO.setBeginDateTime(activity.getBeginDateTime());
        activityBillVO.setEndDateTime(activity.getEndDateTime());
        activityBillVO.setTakeCount(activity.getTakeCount());
        activityBillVO.setStockCount(activity.getStockCount());
        activityBillVO.setStockSurplusCount(null == usedStockCountObj ? activity.getStockSurplusCount() : activity.getStockCount() - Integer.parseInt(String.valueOf(usedStockCountObj)));
        activityBillVO.setStrategyId(activity.getStrategyId());
        activityBillVO.setState(activity.getState());
        activityBillVO.setUserTakeLeftCount(null == userTakeActivityCount ? null : userTakeActivityCount.getLeftCount());

        return activityBillVO;
    }

    @Override
    public int subtractionActivityStock(Long activityId) {
        return activityDao.subtracttionActivityStock(activityId);
    }

    @Override
    public List<ActivityVo> scanTODOAcyivityList(Long id) {
        List<Activity> activityList = activityDao.scanTODOActivityList(id);
        List<ActivityVo> activityVoList = new ArrayList<>(activityList.size());
        for (Activity activity : activityList) {
            ActivityVo activityVo = new ActivityVo();
            activityVo.setId(activity.getId());
            activityVo.setActivityId(activity.getActivityId());
            activityVo.setActivityName(activity.getActivityName());
            activityVo.setBeginDateTime(activity.getBeginDateTime());
            activityVo.setEndDateTime(activity.getEndDateTime());
            activityVo.setState(activity.getState());
            activityVoList.add(activityVo);
        }
        return activityVoList;
    }

    @Override
    public StockResult subtractionActivityStockByRedis(String uId, Long activityId, Integer stockCount, Date endDateTIme) {
        //1.获取抽奖活动库存 Key
        String stockKey = Constants.RedisKey.KEY_LOTTERY_ACTIVITY_STOCK_COUNT(activityId);
        //2.扣减库存，目前占用库存数
        Integer stockUsedCount = (int) redisUtil.incr(stockKey, 1);

        //3.超出库存判断，进行恢复原始库存
        if (stockUsedCount > stockCount) {
            redisUtil.decr(stockKey,1);
            return new StockResult(Constants.ResponseCode.OUT_OF_STOCK.getCode(), Constants.ResponseCode.OUT_OF_STOCK.getInfo());
        }

        //4.以活动库存未占用编号，生成对应加锁Key，细化锁的粒度
        String stockTokenKey = Constants.RedisKey.KEY_LOTTERY_ACTIVITY_STOCK_COUNT_TOKEN(activityId,stockUsedCount);

        long milliseconds = endDateTIme.getTime() - System.currentTimeMillis();
        //5.使用Redistribution.setNx 加一个分布式锁
        boolean lockToken = redisUtil.setNx(stockTokenKey,milliseconds);

        if (!lockToken) {
            logger.info("抽奖活动{}用户秒杀{}扣减库存. 分布式锁失败: {}",activityId,uId,stockTokenKey);
            return new StockResult(Constants.ResponseCode.ERR_TOKEN.getCode() ,Constants.ResponseCode.ERR_TOKEN.getInfo());
        }
        return new StockResult(Constants.ResponseCode.SUCCESS.getCode(), Constants.ResponseCode.SUCCESS.getInfo(),stockTokenKey,stockCount - stockUsedCount);
    }

    @Override
    public void recoverActivityCacheStockByRedis(Long activityId, String tokenKey, String code) {
        if (null == tokenKey) {
            return;
        }
        //删除分布式锁 key
        redisUtil.del(tokenKey);
    }

    @Override
    public ActivityInfoLimitPageRich queryActivityInfoLimitPage(ActivityInfoLimitPageReq req) {
        Long count = activityDao.queryActivityInfoCount(req);
        List<Activity> list = activityDao.queryActivityInfoList(req);

        List<ActivityVo> activityVOList = new ArrayList<>();
        for (Activity activity : list) {
            ActivityVo activityVO = new ActivityVo();
            BeanUtils.copyProperties(activity, activityVO);
            activityVOList.add(activityVO);
        }

        return new ActivityInfoLimitPageRich(count, activityVOList);
    }


}
