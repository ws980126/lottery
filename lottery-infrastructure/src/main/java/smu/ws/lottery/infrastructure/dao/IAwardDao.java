package smu.ws.lottery.infrastructure.dao;

import org.apache.ibatis.annotations.Mapper;
import smu.ws.lottery.infrastructure.po.Award;

import java.util.List;


@Mapper
public interface IAwardDao {

    Award queryAwardInfo(String awardId);

    /**
     * 插入奖品配置
     *
     * @param list 奖品配置
     */
    void insertList(List<Award> list);

}
