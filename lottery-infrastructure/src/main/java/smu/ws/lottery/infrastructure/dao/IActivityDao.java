package smu.ws.lottery.infrastructure.dao;

import org.apache.ibatis.annotations.Mapper;
import smu.ws.lottery.domain.activity.model.req.ActivityInfoLimitPageReq;
import smu.ws.lottery.domain.activity.model.vo.AlterStateVO;
import smu.ws.lottery.infrastructure.po.Activity;

import java.util.List;

@Mapper
public interface IActivityDao {

   void insert(Activity req);

   Activity queryActivityById(Long activityId);

   /**
    * 变更活动状态
    *
    * @param alterStateVO  [activityId、beforeState、afterState]
    * @return 更新数量
    */
   int alterState(AlterStateVO alterStateVO);


   /**
    * 扣减活动库存
    * @param activityId 活动ID
    * @return 更新数量
    */
   int subtracttionActivityStock(Long activityId);


   /**
    *  扫描待处理地活动列表 状态为：通过，活动中
    * @param id
    * @return
    */
    List<Activity> scanTODOActivityList(Long id);

    /**
     *  更新用户领取记录活动后，活动库存
     * @param activity 入参
     */
    void updateActivityStock(Activity activity);

    Long queryActivityInfoCount(ActivityInfoLimitPageReq req);


    List<Activity> queryActivityInfoList(ActivityInfoLimitPageReq req);
}
