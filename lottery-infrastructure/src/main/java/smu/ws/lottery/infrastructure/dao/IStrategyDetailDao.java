package smu.ws.lottery.infrastructure.dao;

import org.apache.ibatis.annotations.Mapper;
import smu.ws.lottery.infrastructure.po.StrategyDetail;

import java.util.List;


@Mapper
public interface IStrategyDetailDao {

    List<StrategyDetail> queryStrategyDetailList(Long strategyId);

    List<String> queryNoStockStrategyAwardList(Long strategyId);

    int deductStock(StrategyDetail req);


    void insertList(List<StrategyDetail> list);

}
