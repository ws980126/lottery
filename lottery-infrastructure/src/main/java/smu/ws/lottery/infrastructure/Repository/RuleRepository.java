package smu.ws.lottery.infrastructure.Repository;
import org.springframework.stereotype.Repository;
import smu.ws.lottery.common.Constants;
import smu.ws.lottery.domain.rule.model.aggregates.TreeRuleRich;
import smu.ws.lottery.domain.rule.model.vo.TreeNodeLineVo;
import smu.ws.lottery.domain.rule.model.vo.TreeNodeVo;
import smu.ws.lottery.domain.rule.model.vo.TreeRootVo;
import smu.ws.lottery.domain.rule.repository.IRuleRepository;
import smu.ws.lottery.infrastructure.dao.RuleTreeDao;
import smu.ws.lottery.infrastructure.dao.RuleTreeNodeDao;
import smu.ws.lottery.infrastructure.dao.RuleTreeNodeLineDao;
import smu.ws.lottery.infrastructure.po.RuleTree;
import smu.ws.lottery.infrastructure.po.RuleTreeNode;
import smu.ws.lottery.infrastructure.po.RuleTreeNodeLine;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class RuleRepository implements IRuleRepository {

    @Resource
    private RuleTreeDao ruleTreeDao;
    @Resource
    private RuleTreeNodeDao ruleTreeNodeDao;
    @Resource
    private RuleTreeNodeLineDao ruleTreeNodeLineDao;

    @Override
    public TreeRuleRich queryTreeRuleRich(Long treeId) {

        // 规则树
        RuleTree ruleTree = (RuleTree) ruleTreeDao.queryRuleTreeByTreeId(treeId);
        TreeRootVo treeRoot = new TreeRootVo();
        treeRoot.setTreeId(ruleTree.getId());
        treeRoot.setTreeRootNodeId(ruleTree.getTreeRootNodeId());
        treeRoot.setTreeName(ruleTree.getTreeName());

        // 树节点->树连接线
        Map<Long, TreeNodeVo> treeNodeMap = new HashMap<>();
        //根据树的ID查询所有节点
        List<RuleTreeNode> ruleTreeNodeList = ruleTreeNodeDao.queryRuleTreeNodeList(treeId);
        for (RuleTreeNode treeNode : ruleTreeNodeList) {
            List<TreeNodeLineVo> treeNodeLineInfoList = new ArrayList<>();
            //对比节点的类型是否是子叶类型
            if (Constants.NodeType.STEM.equals(treeNode.getNodeType())) {
                //根据树的ID和树的的节点ID在NodeLine里面查询
                RuleTreeNodeLine ruleTreeNodeLineReq = new RuleTreeNodeLine();
                ruleTreeNodeLineReq.setTreeId(treeId);
                ruleTreeNodeLineReq.setNodeIdFrom(treeNode.getId());
                List<RuleTreeNodeLine> ruleTreeNodeLineList = ruleTreeNodeLineDao.queryRuleTreeNodeLineList(ruleTreeNodeLineReq);

                for (RuleTreeNodeLine nodeLine : ruleTreeNodeLineList) {
                    TreeNodeLineVo treeNodeLineInfo = new TreeNodeLineVo();
                    treeNodeLineInfo.setNodeIdFrom(nodeLine.getNodeIdFrom());
                    treeNodeLineInfo.setNodeIdTo(nodeLine.getNodeIdTo());
                    treeNodeLineInfo.setRuleLimitType(nodeLine.getRuleLimitType());
                    treeNodeLineInfo.setRuleLimitValue(nodeLine.getRuleLimitValue());
                    treeNodeLineInfoList.add(treeNodeLineInfo);
                }
            }
            TreeNodeVo treeNodeInfo = new TreeNodeVo();
            treeNodeInfo.setTreeId(treeNode.getTreeId());
            treeNodeInfo.setTreeNodeId(treeNode.getId());
            treeNodeInfo.setNodeType(treeNode.getNodeType());
            treeNodeInfo.setNodeValue(treeNode.getNodeValue());
            treeNodeInfo.setRuleKey(treeNode.getRuleKey());
            treeNodeInfo.setRuleDesc(treeNode.getRuleDesc());
            treeNodeInfo.setTreeNodeLineInfoList(treeNodeLineInfoList);

            treeNodeMap.put(treeNode.getId(), treeNodeInfo);
        }

        TreeRuleRich treeRuleRich = new TreeRuleRich();
        treeRuleRich.setTreeRoot(treeRoot);
        treeRuleRich.setTreeNodeMap(treeNodeMap);

        return treeRuleRich;
    }

}
